package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;

import org.json.JSONObject;

/**
 * Created by HP on 3/29/2017.
 */

public class Login extends AppCompatActivity {

    private String username, password;

    private EditText emailEditText;
    private EditText passEditText;
    Button loginBTN;
    TextView regesterTV,forgetPassTXT;
    private CheckBox saveLoginCheckBox;
    Animation animShke;
    String remember = "";


    SQLHelper dbhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);



        inistializaScreen();

saveLoginCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(isChecked=true)
        {
            remember="1";
        }
        else
        {
            remember="0";
        }
    }
});


        regesterTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Login.this,registerScreen.class));


            }
        });

        forgetPassTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(emailEditText.getText().toString().trim().equalsIgnoreCase(""))
                {
                    Toast.makeText(getApplicationContext(),"Please fill your email in the text ",Toast.LENGTH_SHORT).show();
                    emailEditText.startAnimation(animShke);
                    return;
                }
                SessionClass.startSpinwheel(getApplicationContext(),false,true);

                new Thread(new Runnable() {
                    @Override
                    public void run()
                    {
                        try
                        {
                            String urlForget=urlAdressesClass.forgetPAssURL+"?EmailId="+emailEditText.getText().toString().trim()+"&LanguageKey="+SessionClass.languageKey+"&SecurityKey=WEBNAVIMSERVICE";
                            final JSONObject forgetMsgobj=syncJsonClass.getJSONDataObject(urlForget);

                            if(forgetMsgobj!=null)
                            {
                                if(forgetMsgobj.get("Message").equals("Password send to you email"))
                                {

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            Toast.makeText(getApplicationContext(),"Password send to your email, Please check it ",Toast.LENGTH_SHORT).show();
                                           SessionClass.stopSpinWheel();

                                        }
                                    });
                                }
                                else
                                {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            Toast.makeText(getApplicationContext(),"Not Registerd email ",Toast.LENGTH_SHORT).show();
                                           SessionClass.stopSpinWheel();

                                        }
                                    });
                                }

                            }
                            else
                            {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                        Toast.makeText(getApplicationContext(),R.string.noconnection_validation_msg,Toast.LENGTH_SHORT).show();


                                    }
                                });
                            }
                           SessionClass.stopSpinWheel();

                        }
                        catch (Exception xx)
                        {
                            SessionClass.stopSpinWheel();
                        }
                    }
                }).start();

            }
        });

        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if(!checkRule())
                    return;
                SessionClass.startSpinwheel(getApplicationContext(),false,true);
                new Thread(new Runnable() {
                    @Override
                    public void run()
                    {
                        try
                        {
                            String url="";


                            String urlPostData="?"+"UserName="+emailEditText.getText().toString().trim()+"" +
                                    "&Password="+passEditText.getText().toString().trim()+"" +
                                    "&SecurityKey=WEBNAVIMSERVICE" +
                                    "&LanguageKey="+SessionClass.languageKey+"";
                            url=urlAdressesClass.LoginURL+urlPostData;
                            JSONObject data=syncJsonClass.getJSONDataObject(url);
                            if(data!=null)
                            {
                                if(data.getString("IsSuccess").equals("false"))
                                {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run()
                                        {
                                            Toast.makeText(getApplicationContext(),"Wrong email or password ",Toast.LENGTH_SHORT).show();
                                            SessionClass.stopSpinWheel();
                                        }
                                    });
                                }
                                else
                                {
                                    dbhelper=new SQLHelper(getApplicationContext());
                                    dbhelper.open();

                                    String crmID = data.getString("CRMUserId");
                                    SessionClass.userID=crmID;
                                    String FirstName = data.getString("FirstName");
                                    String LastName = data.getString("LastName");
                                    String Gender = data.getString("Gender");
                                    String Age = data.getString("Age");
                                    String MobileNo = data.getString("MobileNo");


                                    dbhelper.Delete("delete from users");
                                    String insertQuery = "insert into users values('" + FirstName + "','" + LastName + "'," +
                                            "'" + emailEditText.getText().toString() + "','" + Gender + "','" + Age + "'," +
                                            "'" + MobileNo + "','" + passEditText.getText().toString().trim() + "','" + crmID + "','"+remember+"','"+SessionClass.language+"','"+SessionClass.userSignUpID+"','Not Available')";

                                String res=    dbhelper.Insert(insertQuery);


                                    SessionClass.stopSpinWheel();
                                    finish();

                                }

                            }
                            else
                            {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run()
                                    {
                                    Toast.makeText(getApplicationContext(),"No Connection try later",Toast.LENGTH_SHORT).show();
                                        SessionClass.stopSpinWheel();
                                    }
                                });
                            }
                          SessionClass.stopSpinWheel();
                        }
                        catch (Exception xxx )
                        {
                            SessionClass.stopSpinWheel();
                            xxx.toString();}
                    }
                }).start();


            }
        });






    }




    private void  inistializaScreen()
    {
        dbhelper=new SQLHelper(this);
        dbhelper.open();

        emailEditText = (EditText) findViewById(R.id.username_edit);
        passEditText = (EditText) findViewById(R.id.password_edit);
        saveLoginCheckBox = (CheckBox) findViewById(R.id.checkBox);
        regesterTV= (TextView) findViewById(R.id.createAccountTV);
        forgetPassTXT= (TextView) findViewById(R.id.forgetPassTXT);
        loginBTN= (Button) findViewById(R.id.loginBTN);
        animShke= AnimationUtils.loadAnimation(this,R.anim.shake);



    }

    private  boolean checkRule()
    {
        if (emailEditText.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), R.string.validation_noemail_msg, Toast.LENGTH_SHORT).show();
            emailEditText.startAnimation(animShke);
            return false;
        }

        if (passEditText.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), R.string.validation_nopass_msg, Toast.LENGTH_SHORT).show();
            passEditText.startAnimation(animShke);
            return false;
        }

        if(!SessionClass.isValidEmaillId(emailEditText.getText().toString().trim()))
        {
            Toast.makeText(getApplicationContext(), R.string.validation_noemail_msg, Toast.LENGTH_SHORT).show();
            emailEditText.startAnimation(animShke);
            return false;
        }
        return  true;
    }



}
