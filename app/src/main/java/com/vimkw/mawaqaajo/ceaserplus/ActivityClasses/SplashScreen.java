package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

/**
 * Created by HP on 3/27/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.Locale;

public class SplashScreen extends Activity
{

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2300;
    TextView verTXT;
    SQLHelper dbhelper;
    RelativeLayout mainLO;
    Animation anim;
    String local;
    String langFlag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        dbhelper=new SQLHelper(getApplicationContext());
        dbhelper.open();

        mainLO= (RelativeLayout) findViewById(R.id.mainSplashLO);
        anim= AnimationUtils.loadAnimation(this,R.anim.slidedown);
        verTXT= (TextView) findViewById(R.id.verNoTXT);
       // verTXT.setText("Version :"+SessionClass.versionNO);

        String loc = "en-US";
        SharedPreferences prefs = getSharedPreferences("languagePref", MODE_PRIVATE);
        String currentLoc = prefs.getString("local", null);
        if (currentLoc != null)
        {
            loc = currentLoc;
        }

        if(loc.equalsIgnoreCase("en-US"))
        {
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;

            SessionClass.language="2";
            SessionClass.languageKey="en_US";

            langFlag="2";
            SessionClass.languageKey="en_US";
        }
        else
        {
            Locale locale = new Locale("ar");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;

            langFlag="1";
            SessionClass.languageKey="ar_KW";
        }
        SessionClass.language=langFlag;

        mainLO.startAnimation(anim);
        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}