package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.ImagesGalleryActivity;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.syncJsonClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.AlbumDetailsAdapter;
import com.vimkw.mawaqaajo.ceaserplus.Others.PhotoGalleryDetails;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by HP on 4/6/2017.
 */

public class PhotoGalleryDetailsFragment extends Fragment {
    private GridView albumDetailsGridView;
    private AlbumDetailsAdapter albumDetailsAdapter;
    private String url = "";

    SQLHelper dbHelper;
    private ArrayList<PhotoGalleryDetails> photoGalleryDetailses;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_gallery_details, container, false);
        String albumId = SessionClass.albumId;
        albumDetailsGridView = (GridView) view.findViewById(R.id.album_details_gallery);

        dbHelper = new SQLHelper(getActivity());
        dbHelper.open();

        SessionClass.startSpinwheel(getContext(),false,true);
        String urlPostData = "?LanguageKey="+SessionClass.languageKey+"&SecurityKey=WEBNAVIMSERVICE&AlbumId=" + albumId + "";

        url = urlAdressesClass.getPhotoGalleryURL + urlPostData;
        syncData sync = new syncData();
        sync.execute();




        return view;
    }


    private class syncData extends AsyncTask<Void, Integer, ArrayList<PhotoGalleryDetails>> {


        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(final ArrayList<PhotoGalleryDetails> galleryArrayList) {
            try {
                SessionClass.stopSpinWheel();

                albumDetailsAdapter = new AlbumDetailsAdapter(getActivity(), galleryArrayList);
                albumDetailsGridView.setAdapter(albumDetailsAdapter);

                albumDetailsGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(getActivity(), ImagesGalleryActivity.class);
                        photoGalleryDetailses = galleryArrayList;
                        SessionClass.galleryDetailsArrayList = photoGalleryDetailses;
                        startActivity(intent);

                    }
                });


            }
            catch (Exception xx)
            {
                SessionClass.stopSpinWheel();
                Toast.makeText(getActivity(),"No Connection",Toast.LENGTH_SHORT).show();

            }
            super.onPostExecute(galleryArrayList);
        }


        @Override
        protected ArrayList<PhotoGalleryDetails> doInBackground(Void... params) {
            ArrayList<PhotoGalleryDetails> galleryArrayList = new ArrayList<>();
            try {
                JSONArray data = syncJsonClass.getJSONDataArray(url);


                for (int i = 0; i < data.length(); i++) {
                    JSONObject photoDetailsObj = data.getJSONObject(i);
                    String photoGalleryDetailsId = photoDetailsObj.getString("Id");
                    String photoGalleryDetailsTitle = photoDetailsObj.getString("Title");
                    String photoGalleryDetailsImage = photoDetailsObj.getString("Image");
                    String photoGalleryDetailsOrder = photoDetailsObj.getString("Order");
                    PhotoGalleryDetails photoGalleryDetails = new PhotoGalleryDetails(photoGalleryDetailsId, photoGalleryDetailsTitle,
                            photoGalleryDetailsImage, photoGalleryDetailsOrder);
                    galleryArrayList.add(photoGalleryDetails);
                }

                return galleryArrayList;


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // callService();
            return galleryArrayList;
        }


    }


}
