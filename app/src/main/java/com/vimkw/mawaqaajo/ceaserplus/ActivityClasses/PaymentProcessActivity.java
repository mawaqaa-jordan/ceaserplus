package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vimkw.mawaqaajo.ceaserplus.R;

/**
 * Created by HP on 4/24/2017.
 */

public class PaymentProcessActivity extends Activity {

    WebView paymentWebview;

    int im;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_process_layout);
        paymentWebview = (WebView) findViewById(R.id.payment_webview);
        String x = SessionClass.paymentURL;
        im = 0;
        SessionClass.startSpinwheel(getApplicationContext(),false,true);

        paymentWebview.getSettings().setJavaScriptEnabled(true);
        paymentWebview.setWebViewClient(new WebViewClient()
        {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(PaymentUrl.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                SessionClass.startSpinwheel(PaymentProcessActivity.this,false,true);

            }


            @Override
            public void onPageFinished(WebView view, String url) {
                try {
                    Log.e("urlOpe", url);
                    im = im + 1;
                    if (im < 4)
                    {


                    }
                    else

                        {
//                        Intent i = new Intent(PaymentProcessActivity.this, Login.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        startActivity(i);

                    }
                 SessionClass.stopSpinWheel();
                }
                catch (Exception xx)
                {
                    SessionClass.stopSpinWheel();
                    xx.toString();
                }

            }

        });
        paymentWebview.loadUrl(x);

    }
}
