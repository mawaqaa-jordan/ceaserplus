package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SendFeedback;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by HP on 3/28/2017.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener {

    TextView fb;
    private GoogleMap mMap;
    //SupportMapFragment mapFragment;
    View v;
    Button facebook, twitter, instgram, youtube;
   boolean mLocationPermissionGranted=true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

          }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        try
        {
            v=inflater.inflate(R.layout.fragment_contact_us, container, false);



        fb = (TextView) v.findViewById(R.id.sendyoufeedback);
        if (SessionClass.language.equalsIgnoreCase("1"))
        {
            fb.setText("ارسل لنا ملاحظاتك ");
        }
        else
            {
            fb.setText("Send Your Feedback");
          }

        fb.setOnClickListener(this);

        facebook = (Button) v.findViewById(R.id.facebook);
        twitter = (Button) v.findViewById(R.id.twitter);
        instgram = (Button) v.findViewById(R.id.instgram);
        youtube = (Button) v.findViewById(R.id.youtube);


        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.facebook.com/Vimkw_-227730574366542/"));
                getActivity().startActivity(i);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://twitter.com/vim_kuwait/"));
                getActivity().startActivity(i);
            }
        });

        instgram.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.instagram.com/vim_kuwait/"));
                getActivity().startActivity(i);
            }
        });

        youtube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.youtube.com/"));
                getActivity().startActivity(i);
            }
        });
            return v;

    }
    catch(Exception xx)
    {
        return null;
    }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onClick(View v) {

        Intent i = new Intent(ContactUsFragment.this.getActivity(), SendFeedback.class);
        startActivity(i);

    }






}
