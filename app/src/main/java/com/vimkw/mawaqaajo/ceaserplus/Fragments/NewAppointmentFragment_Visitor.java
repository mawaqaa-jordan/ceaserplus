package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.app.DatePickerDialog;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.MainActivity;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SendFeedback;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.syncJsonClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.Membership;
import com.vimkw.mawaqaajo.ceaserplus.Others.MembershipAdapter;
import com.vimkw.mawaqaajo.ceaserplus.Others.MembershipBenefit;
import com.vimkw.mawaqaajo.ceaserplus.Others.MembershipBenefitAdapter;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by HP on 4/10/2017.
 */

public class NewAppointmentFragment_Visitor extends Fragment {
    View view;

    EditText fnameTXT,lnameTXT,emailTXT,mobileTXT,commentTXT;
    TextView datetimeTXT;
    Spinner branchSpinner;
    Button submitButton;
    String [] branchArr;

    Calendar myCalendar;
    String url="";


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_appointment_visitor, container, false);
        initView();
        branchArr=getResources().getStringArray(R.array.branches_arr);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, branchArr);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        branchSpinner.setAdapter(dataAdapter);

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };



        datetimeTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });




        branchSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkRule())
                {
                    SessionClass.startSpinwheel(getActivity(),false,true);

                    String urlPostData="FirstName="+fnameTXT.getText().toString().trim()+"" +
                            "&LastName="+lnameTXT.getText().toString().trim()+"" +
                            "&Email="+emailTXT.getText().toString().trim()+"" +
                            "&Mobile="+mobileTXT.getText().toString().trim()+"" +
                            "&PreferredDateTime="+datetimeTXT.getText().toString().trim()+"" +
                            "&Comments="+commentTXT.getText().toString().trim()+"" +
                            "&Branch="+String.valueOf(branchArr[branchSpinner.getSelectedItemPosition()])+"";

                    url= urlAdressesClass.addAppintment_VisitorURL+urlPostData;

                    syncData sync = new syncData();
                    sync.execute();
                }
            }
        });


        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getDate() {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }

    private boolean checkRule() {
        if (fnameTXT.getText().toString().trim().equals("")) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد من الاسم الاول", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the first name", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (mobileTXT.getText().toString().trim().equals("")) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد من رقم الموبايل", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the mobile", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (emailTXT.getText().toString().trim().equals("")) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد من الايميل", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the email", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (commentTXT.getText().toString().trim().equals("")) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد من الملاحظات", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the comments", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (datetimeTXT.getText().toString().trim().equals("")) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد من الاسم التاريخ و الوقت", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the date and time", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (lnameTXT.equals("") ) {
            if(SessionClass.language.equalsIgnoreCase("1")){
                Toast.makeText(getActivity(), "يرجي التاكد الاسم الاخير", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getActivity(), "Please check the last name", Toast.LENGTH_SHORT).show();

            }

            return false;
        }



        return true;
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateLabel() {

        try {
            String myFormat = "MM/dd/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            datetimeTXT.setText(sdf.format(myCalendar.getTime()));
        }
        catch (Exception xx){}
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initView()
    {

        submitButton = (Button) view.findViewById(R.id.submitAppointBTN);
        fnameTXT=(EditText) view.findViewById(R.id.fNameTXT_newapp);
        lnameTXT=(EditText) view.findViewById(R.id.lNameTXT_newapp);
        emailTXT=(EditText) view.findViewById(R.id.emailTXT_newapp);
        mobileTXT=(EditText) view.findViewById(R.id.mobileTXT_newapp);
        commentTXT=(EditText) view.findViewById(R.id.commentTXT_newapp);
        datetimeTXT=(TextView) view.findViewById(R.id.dateTimeTXT_newapp);
        branchSpinner=(Spinner) view.findViewById(R.id.branchSpinner_newapp);



    }

    class syncData extends AsyncTask<Void, Integer, Void>
    {
        String res="";
        @Override
        protected Void doInBackground(Void... params)
        {

            try {

                String data= syncJsonClass.getJSONDataAsString(url);
                if(data !=null)
                {
                    res=data.trim();
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {

                if(res.equalsIgnoreCase("1"))
                {
                    Toast.makeText(getActivity(),"Submit Successfull, Thank you",Toast.LENGTH_SHORT).show();

                }
                else
                {
                    Toast.makeText(getActivity(),"Submit error , please try again",Toast.LENGTH_SHORT).show();
                }
                SessionClass.stopSpinWheel();
                super.onPostExecute(aVoid);
            }
            catch (Exception xx)
            {
                SessionClass.stopSpinWheel();
                Toast.makeText(getActivity(),"Some error , please try later",Toast.LENGTH_SHORT).show();

            }
        }
    }






}
