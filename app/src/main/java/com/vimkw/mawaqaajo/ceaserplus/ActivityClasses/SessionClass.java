package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.widget.ProgressBar;

import com.vimkw.mawaqaajo.ceaserplus.Others.PhotoGalleryDetails;
import com.vimkw.mawaqaajo.ceaserplus.Others.newsClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.packageClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by HP on 4/3/2017.
 */

public class SessionClass
{


    //Osama Section
    public static String userID="";
    public static String oldPAssword="";
    public  static String gender="";
    public  static String country="";
    public  static String surNAme="";
    public  static String popupFlag="";
    public  static int ourClincListPos;
    public  static String versionNO="1.5";
    public  static String language="";
    public  static String languageKey="ar_KW";
    public  static String memberShip_="";
    public  static String subMealID="";
    public  static String userSignUpID="null";
    public  static String memberShipPrice="";
    public  static   ArrayList<String> membershipType;
    public  static String packgePaymentType="";


    public static Dialog spinWheelDialog = null;
    public static void startSpinwheel(Context context, boolean setDefaultLifetime, boolean isCancelable)
    {

        try {


            if (spinWheelDialog != null && spinWheelDialog.isShowing())
                return;
            spinWheelDialog = new Dialog(context, R.style.wait_spinner_style);
            ProgressBar progressBar = new ProgressBar(context);
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            spinWheelDialog.addContentView(progressBar, layoutParams);
            spinWheelDialog.setCancelable(isCancelable);
            spinWheelDialog.show();

            Handler spinWheelTimer = new Handler();
            spinWheelTimer.removeCallbacks(dismissSpinner);
            if (setDefaultLifetime) // If requested for default dismiss time.
                spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + 1000);

            spinWheelDialog.setCanceledOnTouchOutside(false);
        }
        catch (Exception xx){}
    }
    static Runnable dismissSpinner = new Runnable() {

        @Override
        public void run() {
            stopSpinWheel();
        }

    };

    public static void stopSpinWheel() {
        if (spinWheelDialog != null)
            try
            {
                spinWheelDialog.dismiss();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        spinWheelDialog = null;
    }

    public static String CurrentDate(Boolean Time,Boolean Date,Boolean DateTime) {

        String FulDate = null;
        final Calendar c = Calendar.getInstance();
        int hour=c.get(Calendar.HOUR);
        int mint=c.get(Calendar.MINUTE);
        int sec=c.get(Calendar.SECOND);

        int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
        String DateString=String.valueOf(todaysDate);
        String Year=DateString.substring(0, 4);
        String Month=DateString.substring(4, 6);
        String Day=DateString.substring(6, 8);

        if(Date==true)
        {
            FulDate=Year+"-"+Month+"-"+Day;
        }
        else if(Time==true)
        {
            FulDate=hour+":"+mint+":"+sec;
        }
        else if(DateTime==true)
        {
            FulDate=Day+"-"+Month+"-"+Year+" "+hour+":"+mint+":"+sec;
        }
        return(String.valueOf(FulDate));

    }

    public static boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

//    public static void BackupDatabase() throws IOException
//    {
//        boolean success =true;
//
//
//        File file = null;
//        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"CeaserPlusBackup");
//        //file = new File("/sdcard"+File.separator+"CeaserPlusBackup/");
//
//        if(file.exists())
//        {
//            success =true;
//        }
//        else
//        {
//            success = file.mkdir();
//        }
//
//
//        if(success)
//        {
//            String inFileName = "/data/data/com.vimkw.hp.ceaserplus/databases/CEASER_PLUS_DB.s3db";
//            File dbFile = new File(inFileName);
//            FileInputStream fis = new FileInputStream(dbFile);
//
//            String outFileName = Environment.getExternalStorageDirectory()+"/CeaserPlusBackup/CEASER_PLUS_DB.s3db";
//            //Open the empty db as the output stream
//            OutputStream output = new FileOutputStream(outFileName);
//            //transfer bytes from the inputfile to the outputfile
//            byte[] buffer = new byte[1024];
//            int length;
//            while ((length = fis.read(buffer))>0){
//                output.write(buffer, 0, length);
//            }
//
//            output.flush();
//            output.close();
//            fis.close();
//        }
//    }

    public static String Genarate_ID()
    {
        String Final_ID;
        UUID uniqID=UUID.randomUUID();
        String [] MyID=UUID.randomUUID().toString().split("-");
        Final_ID=MyID[0].toString();
        return Final_ID;

    }

    //Waleed Section

    public static String albumId="";
    public static String ImageId="";
    public static ArrayList<PhotoGalleryDetails> galleryDetailsArrayList =new ArrayList<>();
    public static int appPostion=-1;
    public static String tabPostion="";
    public static String memberShip="";
    public static String paymentURL="";

    //Saddam Section

    public static ArrayList<newsClass> newsClassArrayList=new ArrayList<>();
    public static newsClass newsClassobj=new newsClass();


    public static packageClass packageClassObj = new packageClass();

}
