package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.PaymentProcessActivity;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;


public class LiveChatFragment extends Fragment {

    WebView paymentWebview;
    View view;
    int im;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        try
        {

            view = inflater.inflate(R.layout.live_chat_layout, container, false);
            paymentWebview = (WebView) view.findViewById(R.id.payment_webview);

            String x = urlAdressesClass.chatURL;

            im = 0;
            SessionClass.startSpinwheel(getActivity(),false,true);

            paymentWebview.getSettings().setJavaScriptEnabled(true);
            paymentWebview.setWebViewClient(new WebViewClient()
            {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(PaymentUrl.this, description, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {

                    SessionClass.startSpinwheel(getActivity(),false,true);

                }


                @Override
                public void onPageFinished(WebView view, String url) {
                    SessionClass.stopSpinWheel();

                }

            });


            paymentWebview.loadUrl(x);


            return view;
        } catch (Exception xx)
        {
            xx.toString();
            return null;
        }
    }




}
