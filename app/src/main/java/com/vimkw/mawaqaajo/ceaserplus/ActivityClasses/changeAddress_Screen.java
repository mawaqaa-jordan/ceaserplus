package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import org.json.JSONObject;

/**
 * Created by HP on 4/4/2017.
 */


public class changeAddress_Screen extends Activity{

    EditText addres1TXT,address2TXT,address3TXT;
    TextView submitNewAddTV;

    Button submitBTN;
    String url="";
    ProgressDialog progressBar;
    String res="";
    SQLHelper dbhelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.change_addrss_screen);
        initialScreen();

        submitBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
             if(checkRule())
             {
                 progressBar = ProgressDialog.show(changeAddress_Screen.this,"", "Please Wait ...",true,false);
                        new Thread(new Runnable() {
                            @Override
                            public void run()
                            {
                                try
                                {

                                    String urlPostData="?"+"AddressLine1="+addres1TXT.getText().toString().trim()+"" +
                                            "&AddressLine2="+address2TXT.getText().toString().trim()+"" +
                                            "&AddressLine3="+address3TXT.getText().toString().trim()+"" +
                                            "&SecurityKey=WEBNAVIMSERVICE" +
                                            "&CrmUserId="+SessionClass.userID+"" +
                                            "&LanguageKey="+SessionClass.languageKey+"";
                                    url= urlAdressesClass.changeAddressURL+urlPostData;
                                    JSONObject dataObj=syncJsonClass.getJSONDataObject(url);
                                    if(dataObj!=null)
                                    {
                                        final String res=dataObj.getString("ReturnMsg");
                                        if(!res.equalsIgnoreCase(""))
                                        {
                                            dbhelper.Update("update users set address='"+addres1TXT.getText().toString().trim()+"' ");
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run()
                                                {
                                                    Toast.makeText(getApplicationContext(),res,Toast.LENGTH_SHORT).show();
                                                    progressBar.dismiss();
                                                }
                                            });
                                            finish();
                                        }

                                    }
                                    else
                                    {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run()
                                            {
                                                Toast.makeText(getApplicationContext(),R.string.noconnection_validation_msg,Toast.LENGTH_SHORT).show();
                                                progressBar.dismiss();
                                            }
                                        });
                                    }


                                }
                                catch (Exception xx)
                                {
                                    xx.toString();
                                }
                            }
                        }).start();

             }
            }
        });




    }



    private  void  initialScreen()
    {

        dbhelper=new SQLHelper(this);
        dbhelper.open();
        submitNewAddTV= (TextView) findViewById(R.id.send_feedback_txt);


        addres1TXT= (EditText) findViewById(R.id.address1TXT);
        address2TXT= (EditText) findViewById(R.id.address2_TXT);
        address3TXT= (EditText) findViewById(R.id.address3_txt);
        submitBTN= (Button) findViewById(R.id.submitAddressBTN);

    }


    private  boolean checkRule()
    {

        if(addres1TXT.getText().toString().trim().equals("") &&
                address2TXT.getText().toString().trim().equals("") &&
                address3TXT.getText().toString().trim().equals(""))
        {
            Toast.makeText(getApplicationContext(),R.string.address_validation_msg,Toast.LENGTH_SHORT).show();
            return false;
        }



        return true;
    }


}
