package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.TextView;


import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.R;

/**
 * Created by HP on 4/11/2017.
 */

public class dietDetails extends Activity {

    TextView diet_title, mealCalories, mealDescription;
    SQLHelper dbhelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.diet_detail_pop);

        dbhelper=new SQLHelper(getApplicationContext());
        dbhelper.open();

        diet_title = (TextView) findViewById(R.id.mealTitleTXT);
        mealCalories = (TextView) findViewById(R.id.mealCaloriesTXT);
        mealDescription = (TextView) findViewById(R.id.mealDescTXT);

        String query="select MasterMealId,MasterMealName,MasterMenuId,MealType,MealComponentId," +
                "ComponentName,Calorie,MealDesciption,Selected,Day from diet where MealComponentId='"+SessionClass.subMealID+"' " ;

        Cursor data= dbhelper.Select(query,null);
        if(data.moveToFirst())
        {
          diet_title.setText(data.getString(5));
          mealCalories.setText(data.getString(6)+" Cal");
          mealDescription.setText(data.getString(7));

        }




    }
}