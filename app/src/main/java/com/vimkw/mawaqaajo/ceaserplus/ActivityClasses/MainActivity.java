package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.Fragments.AboutUsFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.ContactUsFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.DietFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.FAQFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.HomeFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.LiveChatFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.NewAppointmentFragment_Visitor;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.NewsFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.OurClincFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.PhotoGalleryFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.ProfileFragment;
import com.vimkw.mawaqaajo.ceaserplus.Fragments.missionVisionFragment;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private BottomNavigationView bottomNavigation;
    private Fragment fragment;
    FragmentTransaction tx;

    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    public static EditText searchTXT;
    TextView logOutTxt,langTXT;
    SQLHelper dbhelper;
    Cursor userDataCursor;
    String local;
    String langFlag;


    Boolean doubleBackToExitPressedOnce =false;

    private DrawerLayout drawerLayout;


    @Override
    public void onBackPressed()
    {


        try {

            if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT); //CLOSE Nav Drawer!
            }


            int fragments = getSupportFragmentManager().getBackStackEntryCount();


            if (fragments == 1)
            {
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();


            }
            else

                super.onBackPressed();
        }
        catch (Exception xx)
        {}
    }


    @Override
    protected void onResume()
    {
        try
        {

            String sql =" SELECT remember,crmuserid,password,language from users ";
            userDataCursor =dbhelper.Select(sql, null);
        if(userDataCursor.moveToFirst())
        {

            SessionClass.userID = userDataCursor.getString(1).toString().trim();
            SessionClass.oldPAssword = userDataCursor.getString(2).toString().trim();
            SessionClass.language = userDataCursor.getString(3).toString().trim();
            logOutTxt.setText(R.string.logout_txt);

        }
        else
        {
            SessionClass.userID ="";
            logOutTxt.setText(R.string.signin_txt);
        }



        }
        catch (Exception xx)
        {}
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        logOutTxt = (TextView) findViewById(R.id.log_out_txt);

       // ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);

        dbhelper=new SQLHelper(MainActivity.this);
        dbhelper.open();



        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigation.inflateMenu(R.menu.bottom_menu);
        fragmentManager = getSupportFragmentManager();

        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        if(SessionClass.language.equalsIgnoreCase("1"))
        {
            Locale locale = new Locale("ar");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            navigationView.inflateMenu(R.menu.drawer_menu);

        }
        else
            {
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                navigationView.inflateMenu(R.menu.drawer_menu);

        }


        langTXT= (TextView) findViewById(R.id.langTextBTN);
        if(SessionClass.language.equalsIgnoreCase("1"))
        {
            langTXT.setText("Eng");
        }
        else
        {
            langTXT.setText("AR");
        }

        searchTXT= (EditText) findViewById(R.id.serchTXT);
        searchTXT.setVisibility(View.INVISIBLE);
        searchTXT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                Fragment currentFragment=fragmentManager.findFragmentById(R.id.main_container);
                if(currentFragment!=null) {
                    String[] currentFragmentName = currentFragment.toString().split("\\{");
                    if (currentFragmentName[0].toString().equalsIgnoreCase("NewsFragment")) {
                        NewsFragment.search_news(searchTXT.getText().toString().trim());

                    } else if (currentFragmentName[0].toString().equalsIgnoreCase("OurClincFragment")) {
                        OurClincFragment.search_Clinics(searchTXT.getText().toString().trim());

                    } else if (currentFragmentName[0].toString().equalsIgnoreCase("FAQFragment")) {
                        FAQFragment.search_faq(searchTXT.getText().toString().trim());

                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {


                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.action_d_about_us:

                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new AboutUsFragment();
                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();
                        break;

                    case R.id.action_d_mission:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new missionVisionFragment();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();
                        break;


                    case R.id.action_d_news:
                        searchTXT.setVisibility(View.VISIBLE);
                        fragment = new NewsFragment();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();
                        break;

                    case R.id.action_d_our_clinics:
                        searchTXT.setVisibility(View.VISIBLE);
                        fragment=new OurClincFragment();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();

                        break;

                    case R.id.action_d_my_diet:
                        if(SessionClass.userID.equalsIgnoreCase(""))
                        {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                        else
                        {
                            searchTXT.setVisibility(View.INVISIBLE);
                            fragment = new DietFragment();

                            tx = getSupportFragmentManager().beginTransaction();
                            tx.replace(R.id.main_container, fragment);
                            tx.addToBackStack(null);
                            tx.commit();
                        }
                        break;

//                    case R.id.action_d_user_profile:
//                        searchTXT.setVisibility(View.INVISIBLE);
//                        if(SessionClass.userID.equalsIgnoreCase(""))
//                        {
//                            Intent i = new Intent(getApplicationContext(), Login.class);
//                            startActivity(i);
//                        }
//                        else
//                        {
//                            searchTXT.setVisibility(View.INVISIBLE);
//                            fragment = new ProfileFragment();
//
//                            tx = getSupportFragmentManager().beginTransaction();
//                            tx.replace(R.id.main_container, fragment);
//                            tx.addToBackStack(null);
//                            tx.commit();
//                        }
//                        break;

                    case R.id.action_d_media_gallery:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment=new PhotoGalleryFragment();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();

                        break;

                    case R.id.action_d_new_appointment:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new NewAppointmentFragment_Visitor();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();

                        break;

                    case R.id.action_d_chat:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new LiveChatFragment();
                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();

                        break;



                    case R.id.action_d_faq:
                        searchTXT.setVisibility(View.VISIBLE);
                        fragment = new FAQFragment();

                        tx = getSupportFragmentManager().beginTransaction();
                        tx.replace(R.id.main_container, fragment);
                        tx.addToBackStack(null);
                        tx.commit();

                        break;

//                    default:
//                    Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();


                }
                searchTXT.setText("");

                return  true;
            }
        });


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle
                (this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView)
            {

                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
        //searchTXT.setText("");
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new HomeFragment());
        tx.addToBackStack(null);
        tx.commit();


        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                FragmentTransaction transaction;
                switch (id) {
                    case R.id.action_home:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new HomeFragment();

                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.main_container, fragment).commit();
                        transaction.addToBackStack(null);
                        break;
                    case R.id.action_profile:
                        if(SessionClass.userID.equalsIgnoreCase(""))
                        {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                        else
                        {
                            searchTXT.setVisibility(View.INVISIBLE);
                            fragment = new ProfileFragment();

                            transaction = fragmentManager.beginTransaction();
                            transaction.replace(R.id.main_container, fragment).commit();
                            transaction.addToBackStack(null);
                        }

                        break;
                    case R.id.action_news:
                        searchTXT.setVisibility(View.VISIBLE);
                        fragment = new NewsFragment();

                         transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.main_container, fragment).commit();
                        transaction.addToBackStack(null);
                        break;

                    case R.id.action_contact_us:
                        searchTXT.setVisibility(View.INVISIBLE);
                        fragment = new ContactUsFragment();

                        transaction = fragmentManager.beginTransaction();
                        transaction.replace(R.id.main_container, fragment).commit();
                        transaction.addToBackStack(null);
                        break;



                }
                searchTXT.setText("");

                return true;


            }
        });

        findViewById(R.id.action_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayout.closeDrawer(Gravity.RIGHT);

                } else {
                    drawerLayout.openDrawer(Gravity.RIGHT);
                }
            }
        });

        logOutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SessionClass.userID.equalsIgnoreCase(""))
                {
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    startActivity(i);

                    return;
                }
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Log Out.");
                alertDialog.setMessage("Are You Sure You Want To Log Out ?");
                //  alertDialog.setIcon(R.drawable.ic_launcher);

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      dbhelper.Update("delete from users ");
                        SessionClass.userID="";
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(i);
                    }
                });


                alertDialog.show();


            }
        });

        langTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                final Locale[] locale = new Locale[1];
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Change Language");
                alertDialog.setMessage("Do you want to change language ?");
                //  alertDialog.setIcon(R.drawable.ic_launcher);

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if(langTXT.getText().toString().equalsIgnoreCase("ENG"))
                        {

                            locale[0] = new Locale("en-US");
                            Locale.setDefault(locale[0]);
                            Configuration config = new Configuration();
                            config.locale = locale[0];
                            SessionClass.language="2";
                            SessionClass.languageKey="en_US";
                        }
                        else
                        {
                            locale[0] = new Locale("ar");
                            Locale.setDefault(locale[0]);
                            Configuration config = new Configuration();
                            config.locale = locale[0];

                            SessionClass.language="1";
                            SessionClass.languageKey="ar_KW";
                        }

                        SharedPreferences.Editor editor = getSharedPreferences("languagePref", MODE_PRIVATE).edit();
                        editor.putString("local", locale[0].toString().trim());
                        editor.commit();

                      dbhelper.Update("update users set language='"+SessionClass.language+"'");
                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(i);


                    }
                });


                alertDialog.show();

            }
        });


    }


}
