package com.vimkw.mawaqaajo.ceaserplus.Others;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 4/26/2017.
 */

public class dietDailyMealsClass implements Parcelable
{
    private String mastermealid;
    private String masterMealName;
    private String masterMenuId;
    private String mealType;
    private String mealComponentId;
    private String componentName;
    private String calorie;
    private String mealDesciption;
    private String selected;
    private String day;

    public dietDailyMealsClass(String mastermealid, String masterMealName, String masterMenuId, String mealType, String mealComponentId, String componentName, String calorie, String mealDesciption, String selected, String day) {
        this.setMastermealid(mastermealid);
        this.setMasterMealName(masterMealName);
        this.setMasterMenuId(masterMenuId);
        this.setMealType(mealType);
        this.setMealComponentId(mealComponentId);
        this.setComponentName(componentName);
        this.setCalorie(calorie);
        this.setMealDesciption(mealDesciption);
        this.setSelected(selected);
        this.setDay(day);
    }

    protected dietDailyMealsClass(Parcel in) {
        mastermealid = in.readString();
        masterMealName = in.readString();
        masterMenuId = in.readString();
        mealType = in.readString();
        mealComponentId = in.readString();
        componentName = in.readString();
        calorie = in.readString();
        mealDesciption = in.readString();
        selected = in.readString();
        day = in.readString();
    }

    public static final Creator<dietDailyMealsClass> CREATOR = new Creator<dietDailyMealsClass>() {
        @Override
        public dietDailyMealsClass createFromParcel(Parcel in) {
            return new dietDailyMealsClass(in);
        }

        @Override
        public dietDailyMealsClass[] newArray(int size) {
            return new dietDailyMealsClass[size];
        }
    };

    public String getMastermealid() {
        return mastermealid;
    }

    public void setMastermealid(String mastermealid) {
        this.mastermealid = mastermealid;
    }

    public String getMasterMealName() {
        return masterMealName;
    }

    public void setMasterMealName(String masterMealName) {
        this.masterMealName = masterMealName;
    }

    public String getMasterMenuId() {
        return masterMenuId;
    }

    public void setMasterMenuId(String masterMenuId) {
        this.masterMenuId = masterMenuId;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getMealComponentId() {
        return mealComponentId;
    }

    public void setMealComponentId(String mealComponentId) {
        this.mealComponentId = mealComponentId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    public String getMealDesciption() {
        return mealDesciption;
    }

    public void setMealDesciption(String mealDesciption) {
        this.mealDesciption = mealDesciption;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mastermealid);
        dest.writeString(masterMealName);
        dest.writeString(masterMenuId);
        dest.writeString(mealType);
        dest.writeString(mealComponentId);
        dest.writeString(componentName);
        dest.writeString(calorie);
        dest.writeString(mealDesciption);
        dest.writeString(selected);
        dest.writeString(day);
    }
}
