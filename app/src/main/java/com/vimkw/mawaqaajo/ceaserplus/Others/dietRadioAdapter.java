package com.vimkw.mawaqaajo.ceaserplus.Others;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.dietDetails;
import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.ArrayList;



/**
 * Created by HP on 4/25/2017.
 */

public class dietRadioAdapter extends BaseAdapter {
    private Context context;

    private Activity activity;
    SQLHelper dbhelper;
    ListView listView1;
    private int selectedPosition = -1;
    public static ImageView radio;
    String checked="";
    String unCheckedFlag="";

    private ArrayList<dietDailyMealsClass> mealsArray;
    //private int mSelectedVariation;

    private static LayoutInflater inflater = null;


    public dietRadioAdapter(Activity a, ArrayList<dietDailyMealsClass> mealsArray_,ListView listView) {
        this.activity = a;
        this.mealsArray = mealsArray_;
        this.listView1=listView;


        inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mealsArray.size();
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;


        if (convertView == null) {

            view = inflater.inflate(R.layout.diet_table_list_item, null);
        }

        dbhelper=new SQLHelper(dietRadioAdapter.this.activity);
        dbhelper.open();

        TextView Calories = (TextView) view.findViewById(R.id.diet_table_kal);
        TextView mealName = (TextView) view.findViewById(R.id.mealNameDietTXT);
         radio = (ImageView) view.findViewById(R.id.meal_radioButton);


        dietDailyMealsClass dietClassobj = mealsArray.get(position);


        mealName.setText(dietClassobj.getComponentName());
        Calories.setText(dietClassobj.getCalorie()+" Cal");


        checked = mealsArray.get(position).getSelected().toString();
        if(checked.equalsIgnoreCase("true"))
        {
            radio.setImageResource(R.drawable.radio_active);
        }
        else
        {
            radio.setImageResource(R.drawable.radio);

        }




        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                try
                {
                    dbhelper.Delete("delete from send_diet where  MasterMealId='"+mealsArray.get(position).getMastermealid().toString()+"' ");

                    String query="insert into send_diet values ('"+mealsArray.get(position).getMastermealid().toString()+"'," +
                            "'"+mealsArray.get(position).getMasterMealName().toString()+"'," +
                            "'"+mealsArray.get(position).getMasterMenuId().toString()+"'," +
                            "'"+mealsArray.get(position).getMealType().toString()+"'," +
                            "'"+mealsArray.get(position).getMealComponentId().toString()+"'," +
                            "'"+mealsArray.get(position).getComponentName().toString()+"')";

                    String ressss=  dbhelper.Insert(query);
                    String cc=ressss;

                }
                catch (Exception xx)
                {
                    xx.toString();
                }



                for (int m=0;m<mealsArray.size();m++)
                {
                    mealsArray.get(m).setSelected("false");
                }
                mealsArray.get(position).setSelected("true");
                notifyDataSetChanged();
            }
        });

        mealName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SessionClass.subMealID=mealsArray.get(position).getMealComponentId();
                Intent i = new Intent(dietRadioAdapter.this.activity, dietDetails.class);
                activity.startActivity(i);

            }
        });

        return view;
    }

}