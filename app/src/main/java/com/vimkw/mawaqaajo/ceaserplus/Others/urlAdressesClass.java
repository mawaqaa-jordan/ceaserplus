package com.vimkw.mawaqaajo.ceaserplus.Others;

/**
 * Created by HP on 4/17/2017.
 */

public class urlAdressesClass
{
    static String service_url="http://services.vimkw.com";
    static String securtyKEY="WEBNAVIMSERVICE";

    public  static String LoginURL=                  service_url+"/api/User/Login";
    public  static String registerURL=               service_url+"/api/User/Register";
    public  static String sendFeedURL=               service_url+"/api/Common/SaveContactUs";
    public  static String updatAppoitmentURL=        service_url+"/api/Details/UpdateAppointment";
    public  static String getAppoitmentURL=          service_url+"/api/Details/GetAppointments";
    public  static String newAppoitmentURL=          service_url+"/api/Details/NewAppointment";
    public  static String myDietURL =                service_url+"/api/Details/MyDiet";
    public  static String faqURL =                   service_url+"/api/Common/FAQ";
    public  static String getMembershipTemplateURL = service_url+"/api/Common/GetMembershipTemplate" ;
    public  static String getMembershipURL =         service_url+"/api/Common/GetMembership" ;
    public static String getNewsURL =                service_url+"/api/Common/GetNews";
    public static String getClinicsURL =             service_url+"/api/Common/GetClinics";
    public static String getMyPrescriptionsURL =     service_url+"/api/Details/MyPrescriptions";
    public static String getPhotoGalleryURL =        service_url+"/api/Common/PhotoGallery";
    public static String getPhotoAlbumsURL =         service_url+"/api/Common/PhotoAlbums" ;
    public static String editProfileURL =            service_url+"/api/User/EditProfile" ;
    public static String getProfileURL =             service_url+"/api/User/GetProfile";
    public static String getHomeBannersURL =         service_url+"/api/Common/GetHomeBanners";
    public static String proceedtoPayURL =           service_url+"/api/Details/ProceedtoPay";
    public static String getOffersURL =              service_url+"/api/Common/GetOffers";
    public static String forgetPAssURL =             service_url+"/api/User/ForgotPassword";
    public static String getMembershipTypeURL =      service_url+"/api/Common/GetMembershipType?SecurityKey="+securtyKEY+"&strLang=en_US";
    public static String confirmRegistrationURL =    service_url+"/api/User/ConfirmRegistration";
    public static String changeMealURL =             service_url+"/api/Details/ChangeMeal";
    public static String changeAddressURL =          service_url+"/api/User/ChangeAddress";
    public static String chatURL=                    "https://www.vimkw.com/LiveSupport/UserForm.aspx?busy=true&deptid=&userid=";
    public static String addAppintment_VisitorURL=    service_url+"/api/Details/NewAppointmentReques?";




}
