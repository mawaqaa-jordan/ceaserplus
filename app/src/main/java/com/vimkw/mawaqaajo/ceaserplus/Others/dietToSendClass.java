package com.vimkw.mawaqaajo.ceaserplus.Others;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HP on 5/1/2017.
 */

public class dietToSendClass implements Parcelable
{


    public dietToSendClass(String masterMealId, String masterMealName, String masterMenuId, String mealComponentId, String mealType) {
        this.setMasterMealId(masterMealId);
        setMasterMealName(masterMealName);
        setMasterMenuId(masterMenuId);
        setMealComponentId(mealComponentId);
        setMealType(mealType);
    }
    private  String masterMealId;
    private  String MasterMealName;
    private  String MasterMenuId;
    private  String MealComponentId;
    private  String MealType;

    protected dietToSendClass(Parcel in) {
        masterMealId = in.readString();
        MasterMealName = in.readString();
        MasterMenuId = in.readString();
        MealComponentId = in.readString();
        MealType = in.readString();
    }

    public static final Creator<dietToSendClass> CREATOR = new Creator<dietToSendClass>() {
        @Override
        public dietToSendClass createFromParcel(Parcel in) {
            return new dietToSendClass(in);
        }

        @Override
        public dietToSendClass[] newArray(int size) {
            return new dietToSendClass[size];
        }
    };

    public String getMasterMealId() {
        return masterMealId;
    }

    public void setMasterMealId(String masterMealId) {
        this.masterMealId = masterMealId;
    }

    public String getMasterMealName() {
        return MasterMealName;
    }

    public void setMasterMealName(String masterMealName) {
        MasterMealName = masterMealName;
    }

    public String getMasterMenuId() {
        return MasterMenuId;
    }

    public void setMasterMenuId(String masterMenuId) {
        MasterMenuId = masterMenuId;
    }

    public String getMealComponentId() {
        return MealComponentId;
    }

    public void setMealComponentId(String mealComponentId) {
        MealComponentId = mealComponentId;
    }

    public String getMealType() {
        return MealType;
    }

    public void setMealType(String mealType) {
        MealType = mealType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(masterMealId);
        dest.writeString(MasterMealName);
        dest.writeString(MasterMenuId);
        dest.writeString(MealComponentId);
        dest.writeString(MealType);
    }
}
