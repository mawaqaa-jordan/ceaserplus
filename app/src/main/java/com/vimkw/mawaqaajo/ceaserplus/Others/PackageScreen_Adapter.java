package com.vimkw.mawaqaajo.ceaserplus.Others;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.PackagePayment;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.ArrayList;

/**
 * Created by HP on 4/13/2017.
 */

public class PackageScreen_Adapter extends BaseAdapter {

    private Activity activity;
   // private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;


    ArrayList<packageClass> packageClassArrayList;

    public PackageScreen_Adapter(Activity activity, ArrayList<packageClass> packageClassArrayList) {
        this.activity = activity;
        this.packageClassArrayList = packageClassArrayList;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return packageClassArrayList.size();
    }

    @Override
    public packageClass getItem(int position) {
        packageClass packageClass=packageClassArrayList.get(position);
        return packageClass;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.package_list_row, null);
        }

        TextView title_package = (TextView) vi.findViewById(R.id.title_package);
        TextView ActiveTo = (TextView) vi.findViewById(R.id.ActiveTo);
        TextView ActiveFrom = (TextView) vi.findViewById(R.id.ActiveFrom);
        TextView MembershipFee = (TextView) vi.findViewById(R.id.MembershipFee);
        Button renewBTN=(Button) vi.findViewById(R.id.renewPkgBTN);
        Button upgradeBTN=(Button) vi.findViewById(R.id.upgreadPkgBTN);

        renewBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionClass.packgePaymentType="renew";

                packageClass packageClass = packageClassArrayList.get(position);
                SessionClass.packageClassObj = packageClass;
                Intent intent = new Intent(activity, PackagePayment.class);
                activity.startActivity(intent);
            }
        });

        upgradeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SessionClass.packgePaymentType="upgrade";

                packageClass packageClass = packageClassArrayList.get(position);
                SessionClass.packageClassObj = packageClass;
                Intent intent = new Intent(activity, PackagePayment.class);
                activity.startActivity(intent);
            }
        });

        packageClass packageClassobj = packageClassArrayList.get(position);

        title_package.setText(packageClassobj.getPackages());
        ActiveTo.setText(packageClassobj.getActiveTo());
        ActiveFrom.setText(packageClassobj.getActiveFrom());
        MembershipFee.setText(packageClassobj.getMembershipFee());


        return vi;
    }
}
