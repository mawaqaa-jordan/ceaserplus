package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.Others.NewsDetails_Addapter;
import com.vimkw.mawaqaajo.ceaserplus.Others.newsClass;
import com.vimkw.mawaqaajo.ceaserplus.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HP on 4/9/2017.
 */

public class NewsDetails extends Activity {

    TextView titlee, details;
    ImageView thumb_image;
    String title, Thumbnail, description;

    private ProgressBar pBar;




    NewsDetails_Addapter adapter;

    ArrayList<newsClass> newsClassArr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_news_details);

        newsClass newsClassobj= SessionClass.newsClassobj;

        titlee = (TextView) findViewById(R.id.newDetailTitleTXT);
        details = (TextView) findViewById(R.id.newDetalDescTXT);
        thumb_image = (ImageView) findViewById(R.id.imageView);

        titlee.setText(newsClassobj.getTitle());
        details.setText(Html.fromHtml(newsClassobj.getDescription()) );
        Picasso.with(getApplicationContext()).load(newsClassobj.getThumbnail()).into(thumb_image);


    }



        }
