package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.syncJsonClass;

import com.vimkw.mawaqaajo.ceaserplus.Others.dietRadioAdapter;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.Others.dietDailyMealsClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.dietToSendClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 4/6/2017.
 */

public class DietFragment extends Fragment implements AdapterView.OnItemClickListener {


    ListView saladLV, breakfastLV, soupLV, lunchLV, DinnerLV, snackLV;
    LinearLayout breakfastLO, breakfasttitleLO, breakfastLO2, lunchLO, lunchtitleLO, lunchLO2, DinnerLO, DinnertitleLO, DinnerLO2, snackLO, snacktitleLO, snackLO2;

    ImageView salad_imagearrow, breakfast_imagearrow, soup_imagearrow, lunch_imagearrow, dinner_imagearrow, snack_imagearrow;
    TextView breakfastTV, dinnerTV, lunchTV, snackTV;
    ImageView next, back;
    Button saveBTN;
    String url = "";
    View view;
    Cursor breakfastCurs, snacksCurs, lunchCurs, dinnerCurs;


    String[] daysList = null;

    private static dietRadioAdapter dietMealsAdptr;
    String MealId, MealItem, MasterMealItem, MealPeriod, MealDescription, MealCalories;
    private static Cursor searchCursor;
    static Activity currentCOntx;
    Animation animSlideDOwn, animSlideUp;
    JSONObject mealsJsonObj;

    private static SQLHelper dbHelper;
    dietDailyMealsClass dietMealsClass;
    ArrayList<dietDailyMealsClass> dietDailyMealsClassArrayList;
    ArrayList<dietToSendClass> dietToSendList;

    int counter = 0;

   // String[] a = {"a", "b"};

    TextView daysTV;

    //RequestQueue requestQueue;

    dietDailyMealsClass dietDailyMealsClass_;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.diet_table, container, false);

        dbHelper = new SQLHelper(getActivity());
        dbHelper.open();

        currentCOntx = this.getActivity();


        animSlideDOwn = AnimationUtils.loadAnimation(getActivity(), R.anim.slidedown);
        animSlideUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slideup);

        breakfastTV = (TextView) v.findViewById(R.id.mealType_breakTV);
        dinnerTV = (TextView) v.findViewById(R.id.mealType_dinnerTV);
        lunchTV = (TextView) v.findViewById(R.id.mealType_lunchTV);
        snackTV = (TextView) v.findViewById(R.id.mealType_snackTV);

        next = (ImageView) v.findViewById(R.id.diet_table_arr_right);
        back = (ImageView) v.findViewById(R.id.diet_table_arr_left);
        daysTV = (TextView) v.findViewById(R.id.diet_table_days);

        saveBTN = (Button) v.findViewById(R.id.button_save);
        saveBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SessionClass.startSpinwheel(getContext(),false,true);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String sql = "select MasterMealId,MasterMealName,MasterMenuId,MealComponentId,MealType from send_diet";
                            Cursor dietToSendCurs = dbHelper.Select(sql, null);
                            if (dietToSendCurs.moveToFirst())
                            {
                                dietToSendList = new ArrayList<dietToSendClass>();
                                dietToSendClass dietToSendObj;
                                do {
                                    dietToSendObj = new dietToSendClass(dietToSendCurs.getString(0), dietToSendCurs.getString(1),
                                            dietToSendCurs.getString(2), dietToSendCurs.getString(3), dietToSendCurs.getString(4));
                                    dietToSendList.add(dietToSendObj);
                                }
                                while (dietToSendCurs.moveToNext());

                                if (dietToSendList.size() > 0) {


                                    try {

                                        JSONObject changeMealJsonObject = new JSONObject();
                                        changeMealJsonObject.putOpt("CRMUserId", SessionClass.userID);
                                        changeMealJsonObject.putOpt("MembershipId", "00000000-0000-0000-0000-000000000000");
                                        changeMealJsonObject.putOpt("SecurityKey", "WEBNAVIMSERVICE");
                                        changeMealJsonObject.putOpt("LanguageKey", SessionClass.languageKey);
                                        JSONArray mealComponentsJsonArray = new JSONArray();
                                        for(int x=0;x<dietToSendList.size();x++)
                                        {
                                            JSONObject mealComponentJsonArrayObject = new JSONObject();
                                            mealComponentJsonArrayObject.putOpt("MasterMealId", dietToSendList.get(x).getMasterMealId());
                                            mealComponentJsonArrayObject.putOpt("MasterMealName", dietToSendList.get(x).getMasterMealName());
                                            mealComponentJsonArrayObject.putOpt("MasterMenuId", dietToSendList.get(x).getMasterMenuId());
                                            mealComponentJsonArrayObject.putOpt("MealComponentId", dietToSendList.get(x).getMealComponentId());
                                            mealComponentJsonArrayObject.putOpt("MealType", dietToSendList.get(x).getMealType());
                                            mealComponentsJsonArray.put(mealComponentJsonArrayObject);
                                        }

                                        changeMealJsonObject.putOpt("MealComponents", mealComponentsJsonArray);
                                        Log.e("diets", "Request" + changeMealJsonObject);

                                      String result=  makeJsonObjReq(urlAdressesClass.changeMealURL, changeMealJsonObject);


                                    }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                        SessionClass.stopSpinWheel();
                                    }
//
                                }
                                else
                                {
                                    SessionClass.stopSpinWheel();
                                }
                            }
                            else
                            {
                                SessionClass.stopSpinWheel();
                            }

                        }
                        catch (Exception xx)
                        {
                            SessionClass.stopSpinWheel();
                            xx.toString();
                        }

                    }
                }).start();

            }
        });


        Resources res = getResources();

        daysList = res.getStringArray(R.array.days_table);
        ((TextView) v.findViewById(R.id.diet_table_days)).setText(daysList[counter]);

        Log.e("counter = = ", " " + counter);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                saveBTN.performClick();

                breakfastLO2.setVisibility(View.GONE);
                breakfastLO2.setAnimation(animSlideUp);
                breakfast_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                lunchLO2.setVisibility(View.GONE);
                lunchLO2.setAnimation(animSlideUp);
                lunch_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));


                DinnerLO2.setVisibility(View.GONE);
                DinnerLO2.setAnimation(animSlideUp);
                dinner_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                snackLO2.setVisibility(View.GONE);
                snackLO2.setAnimation(animSlideUp);
                snack_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                counter++;

                if (counter >= daysList.length)
                    counter = 0;

                daysTV.setText(daysList[counter]);
            }
                catch (Exception xx)
            {
                xx.toString();
            }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    breakfastLO2.setVisibility(View.GONE);
                    breakfastLO2.setAnimation(animSlideUp);
                    breakfast_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                    lunchLO2.setVisibility(View.GONE);
                    lunchLO2.setAnimation(animSlideUp);
                    lunch_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));


                    DinnerLO2.setVisibility(View.GONE);
                    DinnerLO2.setAnimation(animSlideUp);
                    dinner_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                    snackLO2.setVisibility(View.GONE);
                    snackLO2.setAnimation(animSlideUp);
                    snack_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));

                    counter--;

                    if (counter < 0)
                        counter = 7 - Math.abs(counter);

                    daysTV.setText(daysList[counter]);
                }
                catch (Exception xx)
                {
                    xx.toString();
                }
            }
        });

        breakfastLO = (LinearLayout) v.findViewById(R.id.breakfastLO);
        lunchLO = (LinearLayout) v.findViewById(R.id.lunchLO);
        DinnerLO = (LinearLayout) v.findViewById(R.id.DinnerLO);
        snackLO = (LinearLayout) v.findViewById(R.id.snackLO);

        breakfasttitleLO = (LinearLayout) v.findViewById(R.id.breakfasttitleLO);
        lunchtitleLO = (LinearLayout) v.findViewById(R.id.lunchtitleLO);
        DinnertitleLO = (LinearLayout) v.findViewById(R.id.dinnertitleLO);
        snacktitleLO = (LinearLayout) v.findViewById(R.id.snacktitleLO);

        breakfast_imagearrow = (ImageView) v.findViewById(R.id.breakfast_imagearrow33);
        lunch_imagearrow = (ImageView) v.findViewById(R.id.lunch_imagearrow);
        dinner_imagearrow = (ImageView) v.findViewById(R.id.dinner_imagearrow);
        snack_imagearrow = (ImageView) v.findViewById(R.id.snack_imagearrow);

        breakfastLO2 = (LinearLayout) v.findViewById(R.id.breakfastLO2);
        breakfastLO2.setVisibility(View.GONE);

        lunchLO2 = (LinearLayout) v.findViewById(R.id.lunchLO2);
        lunchLO2.setVisibility(View.GONE);

        DinnerLO2 = (LinearLayout) v.findViewById(R.id.DinnerLO2);
        DinnerLO2.setVisibility(View.GONE);

        snackLO2 = (LinearLayout) v.findViewById(R.id.snackLO2);
        snackLO2.setVisibility(View.GONE);

        breakfastLV = (ListView) v.findViewById(R.id.breakfastLV);
        breakfastLV.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        lunchLV = (ListView) v.findViewById(R.id.lunchLV);
        lunchLV.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        DinnerLV = (ListView) v.findViewById(R.id.DinnerLV);
        DinnerLV.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        snackLV = (ListView) v.findViewById(R.id.snackLV);
        snackLV.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });


        breakfastLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (breakfastLO2.getVisibility() == View.GONE) {
                    breakfastLO2.setVisibility(View.VISIBLE);
                    breakfastLO2.setAnimation(animSlideDOwn);
                    // breakfasttitleLO.setBackgroundColor(Color.parseColor("#86744e"));
                    breakfast_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));

                    dietDailyMealsClassArrayList = new ArrayList<>();


                    String query = "select MasterMealId,MasterMealName,MasterMenuId,MealType,MealComponentId," +
                            "ComponentName,Calorie,MealDesciption,Selected,Day from diet " +
                            " where mealtype='1' and day='" + daysTV.getText().toString().trim() + "' ";

                    breakfastCurs = dbHelper.Select(query, null);
                    if (breakfastCurs.moveToFirst()) {

                        do {
                            dietDailyMealsClass_ = new dietDailyMealsClass(breakfastCurs.getString(0), breakfastCurs.getString(1),
                                    breakfastCurs.getString(2), breakfastCurs.getString(3), breakfastCurs.getString(4),
                                    breakfastCurs.getString(5), breakfastCurs.getString(6), breakfastCurs.getString(7),
                                    breakfastCurs.getString(8), breakfastCurs.getString(9));
                            dietDailyMealsClassArrayList.add(dietDailyMealsClass_);

                        }
                        while (breakfastCurs.moveToNext());
                    }

                    dietMealsAdptr = new dietRadioAdapter(getActivity(), dietDailyMealsClassArrayList, breakfastLV);
                    breakfastLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    breakfastLV.setAdapter(dietMealsAdptr);

                } else {
                    breakfastLO2.setVisibility(View.GONE);
                    breakfastLO2.setAnimation(animSlideUp);

                    // breakfasttitleLO.setBackgroundColor(Color.parseColor("#CD853F"));
                    breakfast_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));
                }

            }
        });


        lunchLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lunchLO2.getVisibility() == View.GONE) {
                    lunchLO2.setVisibility(View.VISIBLE);
                    lunchLO2.setAnimation(animSlideDOwn);

                    //lunchtitleLO.setBackgroundColor(Color.parseColor("#86744e"));
                    lunch_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));

                    dietDailyMealsClassArrayList = new ArrayList<>();


                    String query = "select MasterMealId,MasterMealName,MasterMenuId,MealType,MealComponentId," +
                            "ComponentName,Calorie,MealDesciption,Selected,Day from diet " +
                            " where mealtype='2' and day='" + daysTV.getText().toString().trim() + "' ";

                    lunchCurs = dbHelper.Select(query, null);
                    if (lunchCurs.moveToFirst()) {

                        do {
                            dietDailyMealsClass_ = new dietDailyMealsClass(lunchCurs.getString(0), lunchCurs.getString(1),
                                    lunchCurs.getString(2), lunchCurs.getString(3), lunchCurs.getString(4),
                                    lunchCurs.getString(5), lunchCurs.getString(6), lunchCurs.getString(7),
                                    lunchCurs.getString(8), lunchCurs.getString(9));
                            dietDailyMealsClassArrayList.add(dietDailyMealsClass_);

                        }
                        while (lunchCurs.moveToNext());
                    }

                    dietMealsAdptr = new dietRadioAdapter(getActivity(), dietDailyMealsClassArrayList, lunchLV);
                    lunchLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    lunchLV.setAdapter(dietMealsAdptr);
                } else {
                    lunchLO2.setVisibility(View.GONE);
                    lunchLO2.setAnimation(animSlideUp);

                    //lunchtitleLO.setBackgroundColor(Color.parseColor("#CD853F"));
                    lunch_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));
                }

            }
        });

        DinnerLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DinnerLO2.getVisibility() == View.GONE) {
                    DinnerLO2.setVisibility(View.VISIBLE);
                    DinnerLO2.setAnimation(animSlideDOwn);

                    // DinnertitleLO.setBackgroundColor(Color.parseColor("#86744e"));
                    dinner_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));

                    dietDailyMealsClassArrayList = new ArrayList<>();


                    String query = "select MasterMealId,MasterMealName,MasterMenuId,MealType,MealComponentId," +
                            "ComponentName,Calorie,MealDesciption,Selected,Day from diet " +
                            " where mealtype='3' and day='" + daysTV.getText().toString().trim() + "' ";

                    dinnerCurs = dbHelper.Select(query, null);
                    if (dinnerCurs.moveToFirst()) {

                        do {
                            dietDailyMealsClass_ = new dietDailyMealsClass(dinnerCurs.getString(0), dinnerCurs.getString(1),
                                    dinnerCurs.getString(2), dinnerCurs.getString(3), dinnerCurs.getString(4),
                                    dinnerCurs.getString(5), dinnerCurs.getString(6), dinnerCurs.getString(7),
                                    dinnerCurs.getString(8), dinnerCurs.getString(9));
                            dietDailyMealsClassArrayList.add(dietDailyMealsClass_);

                        }
                        while (dinnerCurs.moveToNext());
                    }

                    dietMealsAdptr = new dietRadioAdapter(getActivity(), dietDailyMealsClassArrayList, DinnerLV);
                    DinnerLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    DinnerLV.setAdapter(dietMealsAdptr);
                } else {
                    DinnerLO2.setVisibility(View.GONE);
                    DinnerLO2.setAnimation(animSlideUp);

                    // DinnertitleLO.setBackgroundColor(Color.parseColor("#CD853F"));
                    dinner_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));
                }
            }
        });


        snackLO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (snackLO2.getVisibility() == View.GONE) {
                    snackLO2.setVisibility(View.VISIBLE);
                    snackLO2.setAnimation(animSlideDOwn);

                    // snacktitleLO.setBackgroundColor(Color.parseColor("#86744e"));
                    snack_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.up_arrow));

                    dietDailyMealsClassArrayList = new ArrayList<>();

                    String query = "select MasterMealId,MasterMealName,MasterMenuId,MealType,MealComponentId," +
                            "ComponentName,Calorie,MealDesciption,Selected,Day from diet " +
                            " where mealtype='4' and day='" + daysTV.getText().toString().trim() + "' ";

                    snacksCurs = dbHelper.Select(query, null);
                    if (snacksCurs.moveToFirst()) {
                        do {
                            dietDailyMealsClass_ = new dietDailyMealsClass(snacksCurs.getString(0), snacksCurs.getString(1),
                                    snacksCurs.getString(2), snacksCurs.getString(3), snacksCurs.getString(4),
                                    snacksCurs.getString(5), snacksCurs.getString(6), snacksCurs.getString(7),
                                    snacksCurs.getString(8), snacksCurs.getString(9));
                            dietDailyMealsClassArrayList.add(dietDailyMealsClass_);

                        }
                        while (snacksCurs.moveToNext());
                    }

                    dietMealsAdptr = new dietRadioAdapter(getActivity(), dietDailyMealsClassArrayList, snackLV);
                    snackLV.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    snackLV.setAdapter(dietMealsAdptr);

//                    for	(int k=0;k< snacksCurs.getCount();k++)
//          	        {
//	        		 if(snacksCurs.moveToPosition(k))
//	        		 {
//                        if(snacksCurs.getString(8).equalsIgnoreCase("true"))
//                        {
//                            snackLV.setItemChecked(k, true);
//                            break;
//                        }
//	        		 }
//          	       }

                } else {
                    snackLO2.setVisibility(View.GONE);
                    snackLO2.setAnimation(animSlideUp);

                    // snacktitleLO.setBackgroundColor(Color.parseColor("#CD853F"));
                    snack_imagearrow.setImageDrawable(getResources().getDrawable(R.drawable.down_arrow));
                }
            }
        });


        return v;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        SessionClass.startSpinwheel(getContext(),false,true);

        String urlData = "?SecurityKey=WEBNAVIMSERVICE&LanguageKey=" + SessionClass.languageKey + "&UserId="+SessionClass.userID+"";
        url = urlAdressesClass.myDietURL + urlData;
        new GetDiet().execute();


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT).show();
    }


    private class GetDiet extends AsyncTask<Void, Void, Void> {
        String currentDay = "";


        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONArray datajson = syncJsonClass.getJSONDataArray(url);

                if (datajson != null) {
                    dietDailyMealsClassArrayList = new ArrayList<>();
                    dbHelper.Delete("delete from diet");
                    dbHelper.Delete("delete from send_diet");

                    for (int i = 0; i < datajson.length(); i++) {
                        mealsJsonObj = datajson.getJSONObject(i);
                        String masterMealId = mealsJsonObj.getString("MasterMealId");
                        String masterMealName = mealsJsonObj.getString("MasterMealItem");
                        String masterMenuId = mealsJsonObj.getString("MasterMenuId");
                        String mealType = mealsJsonObj.getString("MealTypeId");

                        for (int d = 0; d < daysList.length; d++) {
                            currentDay = daysList[d].toString();
                            JSONArray daysMeals = datajson.getJSONObject(i).getJSONArray(currentDay);
                            if (daysMeals != null) {

                                for (int x = 0; x < daysMeals.length(); x++) {
                                    JSONObject daysMealObj = daysMeals.getJSONObject(x);
                                    String mealComponentId = daysMealObj.getString("ComponentId");
                                    String mealComponentName = daysMealObj.getString("ComponentName");
                                    String calories = daysMealObj.getString("Calorie");
                                    String mealDesc = daysMealObj.getString("MealDesciption");
                                    String selected = daysMealObj.getString("Selected");
                                    String day = currentDay;

                                    dietMealsClass = new dietDailyMealsClass(masterMealId, masterMealName, masterMenuId,
                                            mealType, mealComponentId, mealComponentName, calories, mealDesc, selected, day);

                                    dietDailyMealsClassArrayList.add(dietMealsClass);


                                }
                            }
                        }

                        //String mealComponentId =mealsJsonObj.getString("MasterMealId");
                    }

                }
            } catch (Exception xx) {
                SessionClass.stopSpinWheel();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                if (dietDailyMealsClassArrayList.size() > 0) {
                    dbHelper = new SQLHelper(getActivity());
                    dbHelper.open();
                    for (int i = 0; i < dietDailyMealsClassArrayList.size(); i++) {
                        String qury = "insert into diet " +
                                " values('" + dietDailyMealsClassArrayList.get(i).getMastermealid() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getMasterMealName() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getMasterMenuId() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getMealType() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getMealComponentId() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getComponentName() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getCalorie() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getMealDesciption() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getSelected() + "'," +
                                "'" + dietDailyMealsClassArrayList.get(i).getDay() + "')";

                        String resss = dbHelper.Insert(qury);
                        String cc = "";
                    }

                    Cursor mealTypesNameCurs_break = dbHelper.Select("select distinct MasterMealName from diet where MealType='1'", null);
                    if (mealTypesNameCurs_break.moveToFirst()) {
                        breakfastTV.setText(mealTypesNameCurs_break.getString(0));
                    }

                    Cursor mealTypesNameCurs_lunch = dbHelper.Select("select distinct MasterMealName from diet where MealType='2'", null);
                    if (mealTypesNameCurs_lunch.moveToFirst()) {
                        lunchTV.setText(mealTypesNameCurs_lunch.getString(0));

                    }

                    Cursor mealTypesNameCurs_dinner = dbHelper.Select("select distinct MasterMealName from diet where MealType='3'", null);
                    if (mealTypesNameCurs_dinner.moveToFirst()) {
                        dinnerTV.setText(mealTypesNameCurs_dinner.getString(0));

                    }

                    Cursor mealTypesNameCurs_snack = dbHelper.Select("select distinct MasterMealName from diet where MealType='4'", null);
                    if (mealTypesNameCurs_snack.moveToFirst()) {
                        snackTV.setText(mealTypesNameCurs_snack.getString(0));

                    }


                } else {
                    Toast.makeText(getActivity(), "No connection could not get Data", Toast.LENGTH_LONG).show();

                }
            } catch (Exception xx) {

            }
            SessionClass.stopSpinWheel();
            super.onPostExecute(aVoid);
        }
    }




    private String makeJsonObjReq(String urlPost, JSONObject jsonObject)
    {
        String url =urlPost;
        JsonObjectRequest jsonObjectRequest = null;
        final String[] resultConn = {""};


        try {


            jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject)
                {

                    resultConn[0] =jsonObject.toString();
                    if(jsonObject.toString()!=null && !jsonObject.toString().equalsIgnoreCase(""))
                    {
                        try {


                            JSONObject response = new JSONObject(jsonObject.toString());
                            final String resultmsg = response.getString("ReturnMsg");
                            dbHelper.Delete("delete from send_diet");
                            getActivity().runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    Toast.makeText(getActivity(), resultmsg, Toast.LENGTH_LONG).show();

                                }
                            });
                        }
                        catch (Exception xx)
                        {
                            xx.toString();
                        }
                        SessionClass.stopSpinWheel();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {

                     resultConn[0] =volleyError.toString();
                    getActivity().runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(getActivity(),resultConn[0].toString(), Toast.LENGTH_LONG).show();

                        }
                    });
                    SessionClass.stopSpinWheel();
                }
            });
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    4000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        }
        catch (Exception e)
        {
            SessionClass.stopSpinWheel();

            e.toString();
            return e.toString();
        }
        RequestQueue queue = Volley.newRequestQueue(DietFragment.this.getContext());
        queue.add(jsonObjectRequest);

        return resultConn[0];

    }
}









