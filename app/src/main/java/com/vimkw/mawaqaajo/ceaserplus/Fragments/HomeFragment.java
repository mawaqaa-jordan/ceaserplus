package com.vimkw.mawaqaajo.ceaserplus.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.MainActivity;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.NewsDetails;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.OffersPupup;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.SessionClass;
import com.vimkw.mawaqaajo.ceaserplus.ActivityClasses.syncJsonClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.HomeBannerClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.OfferClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.Others.newsClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HP on 3/28/2017.
 */

public class HomeFragment extends Fragment  {

    SliderLayout sliderLayout, sliderLayout2;
    HashMap<String, String> Hash_file_maps, Hash_file_maps2;
    private String url = "";
    private String url2 = "";
    ArrayList<newsClass> newsArrayList2;
    ArrayList<OfferClass>  offerClassArrayList2;

    SQLHelper dbHelper;
    TextView titletxt, datetxt, disctxt,offerTitleTxt, offerDescTxt,moreDetailTV;;
    ImageView newsImage,offerImage1, offerImage2, offerImage3, offerImage4, offerImage5;;
    int currentPos=0;
    int currentPosOffers=0;
    CountDownTimer timer;
    CountDownTimer timerOffers;
    Activity  activityCurent;
    LinearLayout newsLO;
    ArrayList<newsClass> newsArrayList;



    ArrayList<HomeBannerClass> bannerClassArrayList;


    ImageButton leftNav, rightNav;

    public HomeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            else
            {

            }
        }
        catch (Exception xx){}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_home, container, false);


        activityCurent=HomeFragment.this.getActivity();

        dbHelper = new SQLHelper(getActivity());
        dbHelper.open();

//        leftNav = (ImageButton) v.findViewById(R.id.left_nav);
//        rightNav = (ImageButton) v.findViewById(R.id.right_nav);
      //  mPager = (ViewPager) v.findViewById(R.id.viewpager);
        Hash_file_maps = new HashMap<String, String>();
        Hash_file_maps2 = new HashMap<String, String>();
        sliderLayout = (SliderLayout) v.findViewById(R.id.slider);
        sliderLayout2 = (SliderLayout) v.findViewById(R.id.slider3);
        titletxt = (TextView) v.findViewById(R.id.news_title_txt);
        datetxt = (TextView) v.findViewById(R.id.news_date_txt);
        disctxt = (TextView) v.findViewById(R.id.news_desc_txt);
        newsImage= (ImageView) v.findViewById(R.id.news_image);

        offerImage1 = (ImageView) v.findViewById(R.id.offerImg1);
        offerImage2 = (ImageView) v.findViewById(R.id.offerImg2);
        offerImage3 = (ImageView) v.findViewById(R.id.offerImg3);
        offerImage4 = (ImageView) v.findViewById(R.id.offerImg4);
        offerImage5 = (ImageView) v.findViewById(R.id.offerImg5);
        offerTitleTxt = (TextView) v.findViewById(R.id.offer_title_txt);
        offerDescTxt = (TextView) v.findViewById(R.id.offer_desc_txt);
        MainActivity.searchTXT.setVisibility(View.INVISIBLE);
        moreDetailTV= (TextView) v.findViewById(R.id.showMorenewsTV);
        moreDetailTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try {


                    if (newsArrayList.size() > 0) {
                        String sql = " select Title, news_date, news_desc, thumbnail, more_details, teaser from news" +
                                "  where title='" + titletxt.getText().toString().trim() + "'";
                        Cursor cursor = dbHelper.Select(sql, null);
                        newsClass newsClassOBJ;

                        if (cursor.moveToFirst())
                        {

                            //JSONObject jsonObject = data.getJSONObject(i);
                            String title = cursor.getString(0);
                            String date = cursor.getString(1);
                            String description = cursor.getString(2);
                            String Thumbnail = cursor.getString(3);
                            String MoreDetailsLink = cursor.getString(4);
                            String teaser = cursor.getString(5);

                            newsClassOBJ = new newsClass(title, date, teaser, description, Thumbnail, MoreDetailsLink, "");



                            SessionClass.newsClassobj = newsClassOBJ;
                            Intent intent = new Intent(getActivity(), NewsDetails.class);
                            startActivity(intent);
                        }
                    }
                }
                catch (Exception xx)
                {xx.toString();}
            }
        });
//       newsLO= (LinearLayout) v.findViewById(R.id.newsLayout);
//        newsLO.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                }
//
//        });



        offerImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (offerClassArrayList2.size()>=1) {
                    OffersPupup.offerClassArrayList = offerClassArrayList2;
                    OffersPupup.position = 0;
                    startActivity(new Intent(activityCurent, OffersPupup.class));
                }
                else
                {return;}

            }
        });
        offerImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (offerClassArrayList2.size()>=2)
                {
                    OffersPupup.offerClassArrayList=offerClassArrayList2;
                    OffersPupup.position=1;
                    startActivity(new Intent(activityCurent, OffersPupup.class));
                }
                else
                {return;}

            }
        });

        offerImage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(offerClassArrayList2.size()>=3)
                {
                    OffersPupup.offerClassArrayList=offerClassArrayList2;
                    OffersPupup.position=2;
                    startActivity(new Intent(activityCurent, OffersPupup.class));
                }
                else
                {return;}

            }
        });

        offerImage4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(offerClassArrayList2.size()>=4)
                {
                    OffersPupup.offerClassArrayList=offerClassArrayList2;
                    OffersPupup.position=3;
                    startActivity(new Intent(activityCurent, OffersPupup.class));
                }
                else
                {return;}

            }
        });

        offerImage5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(offerClassArrayList2.size()>=5)
                {
                    OffersPupup.offerClassArrayList=offerClassArrayList2;
                    OffersPupup.position=4;
                    startActivity(new Intent(activityCurent, OffersPupup.class));
                }
                else
                {return;}

            }
        });



        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        SessionClass.startSpinwheel(getContext(),false,true);
        String urlPostData = "?LanguageKey="+SessionClass.languageKey+"&SecurityKey=WEBNAVIMSERVICE";

        url = urlAdressesClass.getHomeBannersURL + urlPostData;

        syncHomeBanner syncData = new syncHomeBanner();
        syncData.execute();

        GetNews getNews = new GetNews();
        getNews.execute();

        GetOffers getOffers=new GetOffers();
        getOffers.execute();

    }



    private class GetOffers extends AsyncTask<Void, Void, ArrayList<OfferClass>> {
        OfferClass offerClass;
        ArrayList<OfferClass> offerClassArrayList;

        @Override
        protected ArrayList<OfferClass> doInBackground(Void... params) {


            try {

                String url = urlAdressesClass.getOffersURL + "?SecurityKey=WEBNAVIMSERVICE&LanguageKey="+SessionClass.languageKey+"";
                JSONArray data = syncJsonClass.getJSONDataArray(url);

                if (data != null) {

                offerClassArrayList = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {

                    JSONObject jsonObject = data.getJSONObject(i);

                    String offerId = jsonObject.getString("OfferId");
                    String title = jsonObject.getString("Title");
                    String imageUrl = jsonObject.getString("ImageUrl");
                    String date = jsonObject.getString("Date");
                    String shortDesc = jsonObject.getString("ShortDesc");
                    String description = jsonObject.getString("Description");

                    offerClass = new OfferClass(offerId, title, imageUrl, date, shortDesc, description);
                    offerClassArrayList.add(offerClass);
                }
                }
                return offerClassArrayList;


            }
            catch (final JSONException e)

            {

            }
            catch (IOException e) {
                e.printStackTrace();
            }


            return null;

        }

        @Override
        protected void onPostExecute(final ArrayList<OfferClass> offerClassArrayList) {
            super.onPostExecute(offerClassArrayList);

            offerClassArrayList2=offerClassArrayList;
            try {
                if (offerClassArrayList2 == null || offerClassArrayList2.size()<0) {
                    Toast.makeText(getActivity(), "No connection could not get Data", Toast.LENGTH_LONG).show();
                    return;
                }

                timerOffers=new CountDownTimer(2000,1900) {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {
                        try{

                                OfferClass offerClass = offerClassArrayList2.get(currentPosOffers);
                                offerTitleTxt.setText(offerClass.getTitle());
                                if(offerClass.getDescription().isEmpty()){
                                    offerDescTxt.setText("Not Available");
                                }
                                else
                                {
                                    offerDescTxt.setText(Html.fromHtml(offerClass.getDescription()));

                                }

                                currentPosOffers++;
                                if(currentPosOffers==4)
                                    currentPosOffers=0;



                        }catch (Exception ee)
                        {
                            currentPosOffers=0;
                        }
                    }

                    @Override
                    public void onFinish()
                    {
                        timerOffers.start();
                    }
                }.start();


                OfferClass offerClass0 = offerClassArrayList.get(0);
                if (!(offerClass0.getImageUrl() == null || offerClass0.getImageUrl().isEmpty()))
                {
                    Picasso.with(activityCurent).load(offerClass0.getImageUrl()).into(offerImage1);

                    offerTitleTxt.setText(offerClass0.getTitle());
                    if(offerClass0.getDescription().isEmpty()){
                        offerDescTxt.setText("Not Available");
                    }
                    else
                    {
                        offerDescTxt.setText(Html.fromHtml(offerClass.getDescription()));

                    }

                }


                OfferClass offerClass1 = offerClassArrayList.get(1);
                if (!(offerClass1.getImageUrl() == null || offerClass1.getImageUrl().isEmpty())) {
                    Picasso.with(activityCurent).load(offerClass1.getImageUrl()).into(offerImage2);
                }


                OfferClass offerClass2 = offerClassArrayList.get(2);
                if (!(offerClass2.getImageUrl() == null || offerClass2.getImageUrl().isEmpty())) {
                    Picasso.with(activityCurent).load(offerClass2.getImageUrl()).into(offerImage3);
                }


//                OfferClass offerClass3 = offerClassArrayList.get(3);
//                if (!(offerClass3.getImageUrl() == null || offerClass3.getImageUrl().isEmpty())) {
//                    Picasso.with(getActivity()).load(offerClass3.getImageUrl()).into(offerImage4);
//                }


//                OfferClass offerClass4 = offerClassArrayList.get(4);
//                if (!(offerClass4.getImageUrl() == null || offerClass4.getImageUrl().isEmpty())) {
//                    Picasso.with(getActivity()).load(offerClass4.getImageUrl()).into(offerImage5);
//                }


              SessionClass.stopSpinWheel();

            }

            catch (Exception ee)
            {
                String sss = "";
                System.out.print("" + ee);

                SessionClass.stopSpinWheel();
            }

        }
    }

    private class syncHomeBanner extends AsyncTask<Void, Integer, ArrayList<HomeBannerClass>> {


        @Override
        protected void onPreExecute() {
           // progressBar.show();
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(ArrayList<HomeBannerClass> bannerClassArrayLis) {

            try {
                HomeBannerClass homeBannerClass;
                for (int i = 0; i < bannerClassArrayLis.size(); i++) {
                    homeBannerClass = bannerClassArrayLis.get(i);
                    Hash_file_maps.put("Ceaser Plus ", homeBannerClass.getImgURL());
                }


                for (String name : Hash_file_maps.keySet()) {

                    TextSliderView textSliderView = new TextSliderView(activityCurent);
                    textSliderView
                            .description("")
                            .image(Hash_file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                            //.setOnSliderClickListener(activityCurent);
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle().putString("extra", name);
                    sliderLayout.addSlider(textSliderView);
                }

                sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);
                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                sliderLayout.setCustomAnimation(new DescriptionAnimation());
                sliderLayout.setDuration(5000);
               // sliderLayout.addOnPageChangeListener(activityCurent);


                super.onPostExecute(bannerClassArrayList);
            }
            catch (Exception xx){}

        }


        @Override
        protected ArrayList<HomeBannerClass> doInBackground(Void... params) {
            ArrayList<HomeBannerClass> bannerClassArrayList = new ArrayList<>();
            HomeBannerClass homeBannerClass;
            try {
                JSONArray data = syncJsonClass.getJSONDataArray(url);

                if (data != null)
                {


                for (int i = 0; i < data.length(); i++) {
                    JSONObject imageUrlObj = data.getJSONObject(i);
                    String imageURL = imageUrlObj.getString("IMageURL");
                    homeBannerClass = new HomeBannerClass(imageURL);
                    bannerClassArrayList.add(homeBannerClass);
                }
               }

                return bannerClassArrayList;


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // callService();
            return null;
        }


    }


    private class GetNews extends AsyncTask<Void, Integer, ArrayList<newsClass>> {

        newsClass newsClassObj;

        @Override
        protected ArrayList<newsClass> doInBackground(Void... params) {
            try {

                String url = urlAdressesClass.getNewsURL + "?SecurityKey=WEBNAVIMSERVICE&LanguageKey="+SessionClass.languageKey+"";
                JSONArray news = syncJsonClass.getJSONDataArray(url);

                if (news != null) {

                newsArrayList = new ArrayList<>();

                dbHelper.Delete("delete from news");
                for (int i = 0; i < news.length(); i++) {

                    JSONObject jsonObject = news.getJSONObject(i);

                    String title = jsonObject.getString("Title");
                    String date = jsonObject.getString("Date");
                    String teaser = jsonObject.getString("Teaser");
                    String description = jsonObject.getString("Description");
                    String Thumbnail = jsonObject.getString("Thumbnail");
                    String MoreDetailsLink = jsonObject.getString("MoreDetailsLink");

                    newsClassObj = new newsClass(title, date, teaser, description, Thumbnail, MoreDetailsLink, "");
                    newsArrayList.add(newsClassObj);

                    String qury = "insert into news(ID,Title, news_date, news_desc, thumbnail, more_details, teaser) values('" +
                            SessionClass.Genarate_ID() + "','" + title + "','" + date + "','" + description + "','" + Thumbnail + "','"
                            + MoreDetailsLink + "','" + teaser + "')";

                    dbHelper.Insert(qury);

                }
            }
                return newsArrayList;

            } catch (final JSONException e)

            {


            } catch ( IOException e)

            {
                e.printStackTrace();
            }


            return null;

        }

        @Override
        protected void onPostExecute(ArrayList<newsClass> newsArrayList) {
            super.onPostExecute(newsArrayList);


            try {
                //From Database
                if (newsArrayList == null || newsArrayList.isEmpty()) {

                    newsClass newsClassOBJ;

                    ArrayList<newsClass> newsClassArrayList1 = new ArrayList<>();

                   // Toast.makeText(getActivity(), "No connection could not get Data", Toast.LENGTH_LONG).show();
                    String sql = " select Title, news_date, news_desc, thumbnail, more_details, teaser from news";
                    Cursor cursor = dbHelper.Select(sql, null);
                    if (cursor.moveToFirst()) {

                        do {

                            //JSONObject jsonObject = data.getJSONObject(i);
                            String title = cursor.getString(0);
                            String date = cursor.getString(1);
                            String description = cursor.getString(2);
                            String Thumbnail = cursor.getString(3);
                            String MoreDetailsLink = cursor.getString(4);
                            String teaser = cursor.getString(5);

                            newsClassOBJ = new newsClass(title, date, teaser, description, Thumbnail, MoreDetailsLink, "");

                            newsClassArrayList1.add(newsClassOBJ);

                        } while (cursor.moveToNext());


                    }

                } else {

                    //From Webservice


                    newsArrayList2=newsArrayList;
                        newsClass newsClassObj = newsArrayList.get(0);
                        datetxt.setText(newsClassObj.getDate());
                        titletxt.setText(Html.fromHtml(newsClassObj.getTitle()));
                        disctxt.setText(Html.fromHtml(newsClassObj.getDescription()));

                timer=new CountDownTimer(2000,2000) {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {
                        try{
                            int size_=newsArrayList2.size();

                            if(currentPos<size_)
                            {
                                newsClass newsClassObj = newsArrayList2.get(currentPos);
                                datetxt.setText(newsClassObj.getDate());
                                titletxt.setText(Html.fromHtml(newsClassObj.getTitle()));
                                disctxt.setText(Html.fromHtml(newsClassObj.getDescription()));
                                Picasso.with(getActivity()).load(newsClassObj.getThumbnail()).into(newsImage);

                                currentPos++;
                            }
                            else if(currentPos==size_)
                            {

                                currentPos=0;
                            }



                        }
                        catch (Exception ee)
                        {

                        }


                    }

                    @Override
                    public void onFinish()
                    {
                        timer.start();
                    }
                }.start();


//                    sliderLayout2.setPresetTransformer(SliderLayout.Transformer.Accordion);
//                    sliderLayout2.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//                    sliderLayout2.setCustomAnimation(new DescriptionAnimation());
//                    sliderLayout2.setDuration(5000);


                    sliderLayout2.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
                        {
                            try{
                                int size_=newsArrayList2.size();

                                if(currentPos<size_)
                                {
                                    newsClass newsClassObj = newsArrayList2.get(currentPos);
                                    datetxt.setText(newsClassObj.getDate());
                                    titletxt.setText(Html.fromHtml(newsClassObj.getTitle()));
                                    disctxt.setText(Html.fromHtml(newsClassObj.getDescription()));
                                    Picasso.with(getActivity()).load(newsClassObj.getThumbnail()).into(newsImage);

                                    currentPos++;
                                }
                                else if(currentPos==size_)
                                    {

                                    currentPos=0;
                                }



                            }catch (Exception ee)
                            {

                            }

                        }

                        @Override
                        public void onPageSelected(int position)
                        {

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });



                }

            } catch (Exception ee) {
                String sss = "";
                System.out.print("" + ee);

            }

        }


    }




}
