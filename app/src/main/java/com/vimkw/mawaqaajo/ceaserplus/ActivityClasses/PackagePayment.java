package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.Others.packageClass;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import org.json.JSONObject;

import java.util.ArrayList;

public class PackagePayment extends Activity {

    TextView payment_title, payment_description, exp_date_payment, price_payment,packgeTYpeTV;
    Button submitBTN;
    RadioButton knetRB, visaRB, masterRB;
    RadioGroup rbGrup;
    String curentCheckdRB = "KNET";

    String url = "";
    ArrayList<String> messagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_payment);

        final packageClass packageClassObj = SessionClass.packageClassObj;


        payment_title = (TextView) findViewById(R.id.payment_title);
        payment_description = (TextView) findViewById(R.id.payment_description);
        exp_date_payment = (TextView) findViewById(R.id.exp_date_payment);
        price_payment = (TextView) findViewById(R.id.price_payment);
        packgeTYpeTV=(TextView) findViewById(R.id.packgeTYpeTV);

        knetRB = (RadioButton) findViewById(R.id.knetRB);
        visaRB = (RadioButton) findViewById(R.id.visaRB);
        masterRB = (RadioButton) findViewById(R.id.masterCardRB);
        rbGrup = (RadioGroup) findViewById(R.id.radioGropBTN);

        submitBTN = (Button) findViewById(R.id.pay_packg_button);

        payment_title.setText(packageClassObj.getPackages());
        exp_date_payment.setText(packageClassObj.getActiveTo());
        price_payment.setText(packageClassObj.getMembershipFee() + " KWD");
        packgeTYpeTV.setText(SessionClass.packgePaymentType);

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SessionClass.startSpinwheel(getApplicationContext(),false,true);

                String urlPostData = "?SecurityKey=WEBNAVIMSERVICE" +
                        "&LanguageKey=" + SessionClass.languageKey + "" +
                        "&CrmUserId=" + SessionClass.userID.toString().trim() + "" +
                        "&Price=" + packageClassObj.getMembershipFee().trim() + "" +
                        "&Id=" + packageClassObj.getId() + "" +
                        "&PaymentMode="+curentCheckdRB+"";

                url = urlAdressesClass.proceedtoPayURL + urlPostData;

                new paymentProcess().execute();


            }
        });

        rbGrup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                String id = String.valueOf(checkedId);
                if(checkedId==knetRB.getId())
                {
                    curentCheckdRB="KNET";
                }
                else if(checkedId==visaRB.getId())
                {
                    curentCheckdRB="VISA";
                }
                else  if(checkedId==masterRB.getId())
                {
                    curentCheckdRB="MASTER";
                }
            }
        });


    }


    private class paymentProcess extends AsyncTask<Void, Integer, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            messagesList = new ArrayList<>();
            try {
                JSONObject data = syncJsonClass.getJSONDataObject(url);
                if (data != null) {
                    String message = data.getString("Message");
                    boolean status = data.getBoolean("Status");
                    String x = "";
                    messagesList.add(message);
                    return messagesList;
                }

            } catch (Exception xx)
            {

            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<String> PaymentRes) {
            try {
               SessionClass.stopSpinWheel();

                if (PaymentRes == null || PaymentRes.isEmpty()) {
                    return;
                }

                 SessionClass.paymentURL = PaymentRes.get(0);

                Intent intent = new Intent(PackagePayment.this, PaymentProcessActivity.class);
                startActivity(intent);

            } catch (Exception xx) {
            }
            super.onPostExecute(PaymentRes);
        }
    }
}
