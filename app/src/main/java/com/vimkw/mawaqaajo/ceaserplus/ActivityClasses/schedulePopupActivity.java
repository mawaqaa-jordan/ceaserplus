package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.vimkw.mawaqaajo.ceaserplus.R;

import java.util.Locale;

/**
 * Created by HP on 4/23/2017.
 */

public class schedulePopupActivity extends Activity {

    Context context;
    TextView startDateText,endDtaeText,scheduleTEXT;
    Button scheduleButton;
    Calendar myCalendar;

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_popup_layout);
        context = getApplicationContext();
        startDateText = (TextView) findViewById(R.id.schedule_start_date_txt);
        endDtaeText = (TextView) findViewById(R.id.schedule_end_date_txt);
        scheduleTEXT = (TextView) findViewById(R.id.schedule_txt);
        if (SessionClass.language.equalsIgnoreCase("1")) {
            scheduleTEXT.setText("إختر  الموعد");
        } else {
            scheduleTEXT.setText("Schedule Date");
        }
        scheduleButton = (Button) findViewById(R.id.schedule_done_btn);
        if (SessionClass.language.equalsIgnoreCase("1")) {
            scheduleButton.setText("تم ");
        } else {
            scheduleButton.setText("DONE");

        }


        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel2();
            }

        };

        startDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(schedulePopupActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        endDtaeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(schedulePopupActivity.this, date2, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });






        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public String getDate() {

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String formattedDate = df.format(c.getTime());
        // formattedDate have current date/time


        // String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        return formattedDate;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateLabel() {

        String myFormat = "yyyy/mm/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        registerScreen.startDateText.setText(sdf.format(myCalendar.getTime()));
        startDateText.setText(sdf.format(myCalendar.getTime()));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateLabel2() {

        String myFormat = "yyyy/mm/dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        registerScreen.endDateText.setText(sdf.format(myCalendar.getTime()));

        endDtaeText.setText(sdf.format(myCalendar.getTime()));
    }
}
