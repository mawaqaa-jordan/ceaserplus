package com.vimkw.mawaqaajo.ceaserplus.ActivityClasses;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vimkw.mawaqaajo.ceaserplus.Others.SQLHelper;
import com.vimkw.mawaqaajo.ceaserplus.Others.urlAdressesClass;
import com.vimkw.mawaqaajo.ceaserplus.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class registerScreen extends AppCompatActivity {

    public static EditText genderCB;
    public static EditText membershipCB;
    public static TextView startDateText,endDateText;
    LinearLayout scheduleLayout;
    TextView scheduleFromTXT,scheduleToTXT,schedulePeriodTXT;
    Button signupBTN;
    EditText firstNameTXT, lastNameTXT, surNameTXT, ageTXT, mobileNoTXT, emailTXT, confirmEmaTXT, passwordTXT, confirmPassTXT;
    SQLHelper dbHelper;
    String url = "";

    RadioGroup schedulePeriod,paymentTypeGroup;
    RadioButton startImmediatelyRBTN, scheduleRBTN,knetRB,visaRB,masterRB;
    boolean startImadiatly=true;
    String paymentMOde="KNET";


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==111)
        {
            if(SessionClass.memberShip.equalsIgnoreCase("Lifestyle Program"))
            {
                paymentTypeGroup.setVisibility(View.VISIBLE);
            }
            else
            {
                paymentTypeGroup.setVisibility(View.GONE);
            }


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
        inistializaScreen();

        //get membershipType from backend
        SessionClass.startSpinwheel(registerScreen.this,false,true);

        new Thread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    String url =urlAdressesClass.getMembershipTypeURL.trim();
                    JSONArray data = syncJsonClass.getJSONDataArray(url);
                    if (data!=null)
                    {
                        SessionClass.membershipType=new ArrayList<String>();
                      for(int i=0;i<data.length();i++)
                      {
                          JSONObject dataObj=data.getJSONObject(i);
                          String memberType=dataObj.getString("TypeName");
                          SessionClass.membershipType.add(memberType);


                      }
                        SessionClass.stopSpinWheel();
                    }
                    else
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                Toast.makeText(registerScreen.this, "Some error in connection please try later", Toast.LENGTH_SHORT).show();
                                SessionClass.stopSpinWheel();
                                 finish();
                            }
                        });
                    }
                    SessionClass.stopSpinWheel();

                }
                catch (Exception xx)
                {
                    SessionClass.stopSpinWheel();
                    xx.toString();
                }
            }
        }).start();

        genderCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionClass.popupFlag = "gender";
                startActivity(new Intent(registerScreen.this, Popup_Class.class));
            }
        });

        membershipCB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionClass.popupFlag="membershipType";
                startActivityForResult(new Intent(registerScreen.this, Popup_Class.class),111);
            }
        });

        startImmediatelyRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleLayout.setVisibility(View.GONE);

            }
        });

        scheduleRBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduleLayout.setVisibility(View.VISIBLE);
                Intent intent = new Intent(registerScreen.this,  schedulePopupActivity.class);
                startActivity(intent);
            }
        });


        signupBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkRule())
                {
                    SessionClass.startSpinwheel(registerScreen.this,false,true);


                        String urlPostData = "?" + "FirstName=" + firstNameTXT.getText().toString().trim() + "" +
                                "&LastName=" + lastNameTXT.getText().toString().trim() + "" +
                                "&SurName=" + surNameTXT.getText().toString().trim() + "" +
                                "&Gender=" + genderCB.getText().toString().trim() + "" +
                                "&Age=" + ageTXT.getText().toString().trim() + "" +
                                "&Email=" + confirmEmaTXT.getText().toString().trim() + "" +
                                "&EntryDate=" + SessionClass.CurrentDate(false, true, false).toString() + "" +
                                "&Password=" + confirmPassTXT.getText().toString().trim() + "" +
                                "&MobileNo=" + mobileNoTXT.getText().toString().trim() + "" +
                                "&MembershipType=" + membershipCB.getText().toString().trim().replace(" ","_") + "" +
                                "&SecurityKey=WEBNAVIMSERVICE" +
                                "&LanguageKey=" + SessionClass.languageKey + "";
                        url = urlAdressesClass.registerURL + urlPostData;

                        syncData sync = new syncData();
                        sync.execute();

                }


            }
        });
    }


    private boolean checkRule() {
        if (firstNameTXT.getText().toString().trim().equals("")) {
            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجي التاكد من الاسم الاول ", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(registerScreen.this, "Please check the First Name", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if (surNameTXT.getText().toString().trim().equals("")) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من اللقب ", Toast.LENGTH_SHORT).show();


            } else {
                Toast.makeText(registerScreen.this, "Please check the Sure Name", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (lastNameTXT.getText().toString().trim().equals("")) {
            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this,"يرجى التاكد من الاسم الاخير", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please check the Last Name", Toast.LENGTH_SHORT).show();

            }

            return false;
        }
        if (genderCB.getText().toString().equals("")) {
            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من الجنس ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please check the Gender", Toast.LENGTH_SHORT).show();
            }

            return false;
        }
        if (ageTXT.getText().toString().trim().equals("")) {
            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من العمر", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please check the Age", Toast.LENGTH_SHORT).show();
            }

            return false;
        }
        if (emailTXT.getText().toString().trim().equals("")) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من الايميل ", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(registerScreen.this, "Please check the Email", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (confirmEmaTXT.getText().toString().trim().equals("")) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من ايميل التاكيد", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please fill confirm Email", Toast.LENGTH_SHORT).show();

            }

            return false;
        }
        if (passwordTXT.getText().toString().trim().equals("")) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من الرقم السري ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please fill  Password", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if (confirmPassTXT.getText().toString().trim().equals("")) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من تاكيد الرقم السري ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please fill confirm Password", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if (!emailTXT.getText().toString().trim().equals(confirmEmaTXT.getText().toString().trim())) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من الايميل ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please cofirm your email", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
        if (!passwordTXT.getText().toString().trim().equals(confirmPassTXT.getText().toString().trim())) {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من الرقم السري ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please cofirm your password", Toast.LENGTH_SHORT).show();


            }
            return false;
        }

        if (!SessionClass.isValidEmaillId(emailTXT.getText().toString().trim())) {

            if (SessionClass.language.equalsIgnoreCase("1")) {

            } else {
            }
            Toast.makeText(registerScreen.this, "Not valid email", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (membershipCB.getText().toString().trim().equals("") )
        {

            if (SessionClass.language.equalsIgnoreCase("1")) {
                Toast.makeText(registerScreen.this, "يرجى التاكد من نوع العضوية ", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(registerScreen.this, "Please check the membership type", Toast.LENGTH_SHORT).show();


            }
            return false;
        }


        return true;
    }

    private void inistializaScreen() {
        dbHelper = new SQLHelper(this);
        dbHelper.open();

        scheduleLayout= (LinearLayout) findViewById(R.id.schedule_layout);
        startDateText= (TextView) findViewById(R.id.schedule_start_date_txt);
        endDateText= (TextView) findViewById(R.id.schedule_end_date_txt);
        schedulePeriodTXT= (TextView) findViewById(R.id.schedule_period_txt);
        genderCB= (EditText) findViewById(R.id.genderCbox);
        membershipCB= (EditText) findViewById(R.id.membership_type);
        signupBTN= (Button) findViewById(R.id.regsterBTN);

        firstNameTXT= (EditText) findViewById(R.id.firstNameTXT);
        lastNameTXT= (EditText) findViewById(R.id.lastNameTXT);
        surNameTXT= (EditText) findViewById(R.id.sureNameTXT);
        ageTXT= (EditText) findViewById(R.id.ageCB);
        mobileNoTXT= (EditText) findViewById(R.id.mobileTXT);
        emailTXT= (EditText) findViewById(R.id.emailTXT);
        confirmEmaTXT= (EditText) findViewById(R.id.confirmEmailTXT);
        passwordTXT= (EditText) findViewById(R.id.passswordTXT);
        confirmPassTXT= (EditText) findViewById(R.id.confirmaPassTXT);



        knetRB= (RadioButton) findViewById(R.id.knetRB2);
        visaRB= (RadioButton) findViewById(R.id.visaRB2);
        masterRB= (RadioButton) findViewById(R.id.masterCardRB2);
        paymentTypeGroup= (RadioGroup) findViewById(R.id.radioGrop2);
        paymentTypeGroup.setVisibility(View.GONE);
        paymentTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId)
            {
                if(checkedId==knetRB.getId())
                {
                    paymentMOde="KNET";
                }
                else if(checkedId==visaRB.getId())
                {
                    paymentMOde="VISA";
                }
                else  if(checkedId==masterRB.getId())
                {
                    paymentMOde="MASTER";
                }
            }
        });

        if(SessionClass.language.equalsIgnoreCase("1")){
            schedulePeriodTXT.setText("فترة العضوية");
        }else{
            schedulePeriodTXT.setText("Schedule Period");
        }

        scheduleFromTXT= (TextView) findViewById(R.id.schedule_From_txt);
        if(SessionClass.language.equalsIgnoreCase("1")){
            scheduleFromTXT.setText("من ");
        }else{
            scheduleFromTXT.setText("FROM");
        }

        scheduleToTXT= (TextView) findViewById(R.id.schedule_to_txt);
        if(SessionClass.language.equalsIgnoreCase("1")){
            scheduleToTXT.setText("الى ");
        }else{
            scheduleToTXT.setText("To");
        }



        schedulePeriod = (RadioGroup) findViewById(R.id.schedule_period_radio_group);
        startImmediatelyRBTN = (RadioButton) findViewById(R.id.Start_Immediately_radio_button);
        if (SessionClass.language.equalsIgnoreCase("1"))
        {
            startImmediatelyRBTN.setText("بدأ الان ");
        } else
            {
            startImmediatelyRBTN.setText("Start Immediately");
        }
        startImmediatelyRBTN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked==true)
                {
                    startImadiatly=true;
                }
                else
                {
                    startImadiatly=false;
                }
            }
        });
        scheduleRBTN = (RadioButton) findViewById(R.id.Schedule_radio_button);
        if (SessionClass.language.equalsIgnoreCase("1")) {
            scheduleRBTN.setText("جدول زمني");
        } else {
            scheduleRBTN.setText("Schedule");
        }
    }

    class syncData extends AsyncTask<Void, Integer, Void> {


        String res = "";
        String crmUser = "";
        String memberhiptype=membershipCB.getText().toString().trim();

        @Override
        protected void onPreExecute() {
            // progressBar.show();
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(Void result) {
            try {
                if (res.equalsIgnoreCase("Success"))
                {
                    if (SessionClass.language.equalsIgnoreCase("1")) {
                        Toast.makeText(registerScreen.this, "تم التسجيل بنجاح ", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(registerScreen.this, "Registerd successfull ", Toast.LENGTH_SHORT).show();

                    }

                    if(memberhiptype.equalsIgnoreCase("Lifestyle Program"))
                    {

                        new Thread(new Runnable() {
                            @Override
                            public void run()
                            {
                                try
                                {
                                    String confirmURL="";
                                   if (startImadiatly==false)
                                   {
                                       String startDate=startDateText.getText().toString().trim();
                                       String endDate=endDateText.getText().toString().trim();

                                       String confirmURLStartImditly=urlAdressesClass.confirmRegistrationURL+"?SecurityKey=WEBNAVIMSERVICE" +
                                               "&UserSignupId="+SessionClass.userSignUpID+"" +
                                               "&Price="+SessionClass.memberShipPrice+"" +
                                               "&StartImmediate="+startImadiatly+"" +
                                               "&StartDate="+startDate+"" +
                                               "&EndDate="+endDate+"" +
                                               "&PaymentMode="+paymentMOde+"";
                                       confirmURL=confirmURLStartImditly;
                                   }
                                   else
                                   {

                                       String confirmURLWithDate=urlAdressesClass.confirmRegistrationURL+"?SecurityKey=WEBNAVIMSERVICE" +
                                               "&UserSignupId="+SessionClass.userSignUpID+"" +
                                               "&Price="+SessionClass.memberShipPrice+"" +
                                               "&StartImmediate="+startImadiatly+"" +
                                               "&PaymentMode="+paymentMOde+"";
                                       confirmURL=confirmURLWithDate;
                                   }
                                    JSONObject dataPayment = syncJsonClass.getJSONDataObject(confirmURL);
                                    if(dataPayment!=null)
                                    {
                                        SessionClass.paymentURL=dataPayment.getString("ReturnUrl").toString().trim();
                                        Intent intent = new Intent(registerScreen.this, PaymentProcessActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                                catch (Exception xx)
                                {
                                    xx.toString();
                                }

                            }
                        }).start();

                    }else {
                        SessionClass.stopSpinWheel();
                        Intent i = new Intent(registerScreen.this, Login.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(i);

                    }
                    SessionClass.stopSpinWheel();

                }
                else
                    {

                    if (SessionClass.language.equalsIgnoreCase("1")) {
                        Toast.makeText(registerScreen.this, "اسم المتسخدم مسجل مسبقا ", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(registerScreen.this, "You are already registerd ", Toast.LENGTH_SHORT).show();

                    }
                }

                SessionClass.stopSpinWheel();



            } catch (Exception ee) {
                SessionClass.stopSpinWheel();

            }
            super.onPostExecute(result);
        }


        @Override
        protected Void doInBackground(Void... params) {

            try {
                //JSONArray data= syncJsonClass.getJSONDataArray(url);
                JSONObject data = syncJsonClass.getJSONDataObject(url);
                if (data != null)
                {
                       SessionClass.userSignUpID=data.getString("UserSignupId").toString();
                       SessionClass.memberShipPrice=data.getString("Price").toString();
                       res = data.getString("Status").toString();
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


    }
}
